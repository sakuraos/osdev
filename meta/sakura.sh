#!/usr/bin/env bash

shopt -s nocasematch

help(){
    echo ""
    echo "Usage: $0 COMMAND [OPTIONS]"
    echo ""
    echo "A script to semi-automate the building and testing of sakuraos"
    echo ""
    echo "Commands:"
    echo "  toolchain       Build the toolchain"
    echo "  build           Build the os"
    echo "  build-debug     Build the os with all debug info enabled"
    echo "  run             Run the image in qemu"
    echo "  run-debug       Run the image in qemu with debug flags and start gdb"
    echo "  help            Display this message and exit"
    echo ""
    echo "Options:"
    echo "  -a <arch>       Specify the architecture to target (defaults to i386)"
    echo "  -t <toolchain>  Specify the toolchain to use for building (defaults to gcc)"
    echo "  -b <bootloader> Specify the bootloader to generate the image with (defaults to grub)"
}

first-build(){
    TLD=$(git rev-parse --show-toplevel)

    push-tld

    if [ ! -d build ] ;then
        mkdir build
    fi

    sed $'s|@TLD@|'"${TLD}"$'|g' \
        toolchain/cmake/cross-"${ARCH}"-"${TOOLCHAIN}".cmake.in \
        > build/cross-"${ARCH}"-"${TOOLCHAIN}".cmake

    if [ "${TOOLCHAIN}" = "gcc" ] ;then
        ./toolchain/local/"${ARCH}"/bin/"${ARCH}"-pc-sakura-gcc -ffreestanding \
            -nostdlib -o kernel/arch/"${ARCH}"/crti.o \
            -c kernel/arch/"${ARCH}"/crti.s
        ./toolchain/local/"${ARCH}"/bin/"${ARCH}"-pc-sakura-gcc -ffreestanding \
            -nostdlib -o kernel/arch/"${ARCH}"/crtn.o \
            -c kernel/arch/"${ARCH}"/crtn.s
    fi

    if [ "${BOOTLOADER}" = "grub" ] ;then
        if [ ! -d build/"$ARCH"/root/boot/grub ] ;then
            mkdir -p build/"$ARCH"/root/boot/grub
        fi
        cp meta/grub/* build/"$ARCH"/root/boot/grub/
    fi

    cmake -G"Ninja Multi-Config" -Bbuild \
        -DCMAKE_TOOLCHAIN_FILE=build/cross-"${ARCH}"-"${TOOLCHAIN}".cmake \
        -DCMAKE_INSTALL_PREFIX=build/"${ARCH}"/root -DARCH="${ARCH}" \
        -DCMAKE_EXPORT_COMPILE_COMMANDS=YES .

    pop-tld
}

pushd-safe(){
    trap "popd || exit" SIGINT SIGTERM
    pushd $1 || exit
}

popd-safe(){
    popd || exit
    trap - SIGINT SIGTERM
}

push-tld(){
    TLD=$(git rev-parse --show-toplevel)
    if [ "${TLD}" != "$(pwd)" ] ;then
        pushd-safe "$TLD"
    fi
}

pop-tld(){
    TLD=$(git rev-parse --show-toplevel)
    if [ "${TLD}" = "${DIRSTACK[0]}" ] && [ ${#DIRSTACK[@]} -gt 1 ] ;then
        popd-safe
    fi
}

buildstep(){
    NAME=$1
    shift
    "$@" 2>&1 | sed $'s|^|\x1b[34m['"${NAME}"$']\x1b[39m |'
}

copy-headers(){
    push-tld
    mkdir -p "build/${ARCH}"
    pushd "build/${ARCH}" || exit
        mkdir -p root/usr/include/
        SRC_ROOT=$(git rev-parse --show-toplevel)
        FILES=$(find "$SRC_ROOT/libs/libc" "$SRC_ROOT/libs/libm" -name '*.h' -print)
        for HEADER in $FILES; do
            TARGET=$(echo "$HEADER" | sed -e "s|$SRC_ROOT/libs/libc/include||" -e "s|$SRC_ROOT/libs/libm/include||")
            buildstep "system_headers" $INSTALL -D "$HEADER" "root/usr/include/$TARGET"
        done
        unset SRC_ROOT
    popd || exit
    pop-tld
}

INSTALL="install"

COMMAND=$1
shift

while getopts ":ha:t:b:" option; do
    case $option in
        h)
            help
            exit;;
        a)
            case $OPTARG in
                "i386"|"i686"|"x86")
                    ARCH=i686;;
                *)
                    echo "Error: Unsupported architecture $OPTARG"
                    help
                    exit;;
            esac;;
        t)
            case $OPTARG in
                "gcc")
                    TOOLCHAIN=gcc;;
               # "clang"|"llvm")
               #     TOOLCHAIN=llvm;;
                *)
                    echo "Error: Unsupported toolchain $OPTARG"
                    help
                    exit;;
            esac;;
        b)
            case $OPTARG in
                "grub")
                    BOOTLOADER=grub;;
               # "clang"|"llvm")
               #     TOOLCHAIN=llvm;;
                *)
                    echo "Error: Unsupported toolchain $OPTARG"
                    help
                    exit;;
            esac;;
        \?)
            echo "Error: Invalid option $OPTARG"
            help
            exit;;
    esac
done

ARCH=${ARCH:-i686}
TOOLCHAIN=${TOOLCHAIN:-gcc}
BOOTLOADER=${BOOTLOADER:-grub}

case $COMMAND in
    "toolchain")
        PWD=$(pwd)
        TLD=$(git rev-parse --show-toplevel)
        echo "PWD=$PWD"
        echo "TLD=$TLD"
        ;;
    "build")
        push-tld
        if [ ! -d build ] ;then
            copy-headers
            first-build
        fi
        cmake --build build --config Release -j1 -v
        pop-tld
        ;;
    "build-debug")
        push-tld
        if [ ! -d build ] ;then
            copy-headers
            first-build
        fi
        cmake --build build --config Debug -j1 -v
        pop-tld
        ;;
    "run")
        push-tld
        pushd-safe build

        qemu-system-i386 -device virtio-serial -chardev stdio,id=stdout,mux=on\
            -device virtconsole,chardev=stdout -serial chardev:stdout\
            -kernel kernel/Release/Kernel -append serial-debug

        popd-safe
        pop-tld
        ;;
    "run-debug")
        push-tld
        pushd-safe build

        qemu-system-i386 -device virtio-serial -chardev stdio,id=stdout,mux=on\
            -device virtconsole,chardev=stdout -serial chardev:stdout\
            -kernel kernel/Release/Kernel -append serial-debug -s -S -d int -no-reboot

        popd-safe
        pop-tld
        ;;
    "help")
        help
        exit;;
    *)
        echo "Error: Unknown command $2"
        help
        exit;;
esac
