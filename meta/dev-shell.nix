with import <nixpkgs> {};

stdenv.mkDerivation{
  name = "cpp-env";
  nativeBuildInputs=[
    mpfr
    gmp
    libmpc
  ];
  hardeningDisable=["format" "fortify"];
}
