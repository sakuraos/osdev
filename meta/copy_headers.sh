#!/usr/bin/env bash

DIR="$(git rev-parse --show-toplevel)"
BUILD="$DIR/build"

REALPATH="realpath"
INSTALL="install"

buildstep() {
NAME=$1
shift
"$@" 2>&1 | sed $'s|^|\x1b[34m['"${NAME}"$']\x1b[39m |'
}

mkdir -p "$BUILD/i686"
pushd "$BUILD/i686"
    mkdir -p root/usr/include/
    SRC_ROOT=$($REALPATH "$DIR")
    FILES=$(find "$SRC_ROOT"/libs/libc "$SRC_ROOT"/libs/libm -name '*.h' -print)
    for header in $FILES; do
        target=$(echo "$header" | sed -e "s@$SRC_ROOT/libs/libc/include@@" -e "s@$SRC_ROOT/libs/libm/include@@")
        buildstep "system_headers" $INSTALL -D "$header" "root/usr/include/$target"
    done
    unset SRC_ROOT
popd
