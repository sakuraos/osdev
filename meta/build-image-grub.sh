#!/usr/bin/env bash

if (( $# == 0 )); then
    echo "Error: No kernel provided"
    exit
fi

KERNEL_PATH=$1

TLD=$(git rev-parse --show-toplevel)
pushd "$TLD"

pushd build

if [ ! -d mnt ] ;then
    mkdir mnt
fi

DISK_SIZE=$(du -sb $ARCH/root | cut -f1)                 #get the amount of space needed for everything on the disk
INODE_COUNT=$(du --inodes -s $ARCH/root | cut -f1)       #get the inodes used for the disk

KERNEL_SIZE=$(du -sb $KERNEL_PATH | cut -f1)            #get the size of the kernel
KERNEL_INODES=$(du --inodes -s $KERNEL_PATH | cut -f1)  #get the number of inodes used by the kernel(should be one. just adding for completeness)

GRUB_SPACE=$((1024*1024))                               #add 1MiB of space for grub
GRUB_INODES=$(((GRUB_SPACE+1024-1)/1024))               #add 1 inode per KiB of space added

EXTRA_SPACE=$((20*1024*1024))                           #add 10MiB of extra space
EXTRA_INODES=$(((EXTRA_SPACE+1024-1)/1024))             #add 1 inode per KiB of space added

#echo "DISK_SIZE=$DISK_SIZE"
#echo "INODE_COUNT=$INODE_COUNT"
#echo "KERNEL_SIZE=$KERNEL_SIZE"
#echo "KERNEL_INODES=$KERNEL_INODES"

if [ $(((DISK_SIZE+KERNEL_SIZE+EXTRA_SPACE+GRUB_SPACE+1024-1)/1024)) -lt $((INODE_COUNT+KERNEL_INODES+EXTRA_INODES+GRUB_INODES)) ] ;then
    USE_INODE=1
    echo "More inodes needed than 1 per KiB. Using inode count for disk size"
fi

if [ -z $USE_INODE ] ;then
    TOTAL_SIZE=$((DISK_SIZE+KERNEL_SIZE+EXTRA_SPACE+GRUB_SPACE))
else
    TOTAL_SIZE=$(((INODE_COUNT+KERNEL_INODES+EXTRA_INODES+GRUB_INODES)*2*1024))
fi

echo "TOTAL_SIZE=$TOTAL_SIZE"

NUM_SECTORS=$(((TOTAL_SIZE+512-1)/512))

echo "NUM_SECTORS=$NUM_SECTORS"
if [ -e disk.img ] ;then
    if [ ! $(du -sb disk.img | cut -f1) -eq $((NUM_SECTORS * 512)) ] ;then
        echo "Disk image is the wrong size. Removing..."
        rm disk.img
    fi
fi

if [ ! -e disk.img ] ;then
    echo "Creating new disk image"
    dd if=/dev/zero of=disk.img bs=512 count=$NUM_SECTORS

    parted disk.img mklabel msdos mkpart primary 2048s $((NUM_SECTORS - 1))s set 1 boot on print

    CREATE_FS=1

fi

sudo losetup /dev/loop10 disk.img
sudo losetup /dev/loop11 disk.img -o 1048576

if [ ! -z $CREATE_FS ] ;then
    sudo mkfs.ext2 /dev/loop11
fi

sudo mount /dev/loop11 mnt

sudo cp -r $ARCH/root/* mnt
sudo cp $KERNEL_PATH mnt/boot

sudo grub-install --boot-directory=mnt/boot --no-floppy \
    --modules="normal part_msdos ext2 multiboot" --themes="" \
    --locales="en@quot" /dev/loop10

sudo umount mnt

sudo losetup -d /dev/loop11
sudo losetup -d /dev/loop10

popd
popd
