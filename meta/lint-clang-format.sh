#!/usr/bin/env bash

clang-format -i -style=file $(find . \( -name tarballs -o -name build -o -name local -o -name CMakeFiles \) -prune -o \( -name '*.cpp' -o -name '*.c' -o -name '*.h' \)) || exit 0
