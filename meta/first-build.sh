#!/usr/bin/env bash

TLD=$(git rev-parse --show-toplevel)

ARCH=${ARCH:-i686}
TOOLCHAIN=${TOOLCHAIN:-gcc}

if [ ! -d build ] ;then
    mkdir build
fi

sed $'s|@TLD@|'"${TLD}"$'|g' toolchain/cmake/cross-${ARCH}-${TOOLCHAIN}.cmake.in > build/cross-${ARCH}-${TOOLCHAIN}.cmake

if [ $TOOLCHAIN == "gcc" ]; then
    ./toolchain/local/${ARCH}/bin/${ARCH}-pc-sakura-gcc -ffreestanding -nostdlib -o kernel/arch/${ARCH}/crti.o -c kernel/arch/${ARCH}/crti.s
    ./toolchain/local/${ARCH}/bin/${ARCH}-pc-sakura-gcc -ffreestanding -nostdlib -o kernel/arch/${ARCH}/crtn.o -c kernel/arch/${ARCH}/crtn.s
fi

cmake -GNinja -Bbuild -DCMAKE_TOOLCHAIN_FILE=build/cross-${ARCH}-${TOOLCHAIN}.cmake -DCMAKE_INSTALL_PREFIX=build/i686/root -DARCH=${ARCH} \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=YES .

ninja -C build
