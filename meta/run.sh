#!/bin/sh

qemu-system-i386 -device virtio-serial -chardev stdio,id=stdout,mux=on\
 -device virtconsole,chardev=stdout -serial chardev:stdout\
 -kernel kernel/Kernel -append serial-debug
