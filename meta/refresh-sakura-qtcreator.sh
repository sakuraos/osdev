#!/bin/sh

set -e

if [ -z "$SAKURA_SOURCE_DIR" ]
then
    SAKURA_SOURCE_DIR="$(git rev-parse --show-toplevel)"
    echo "Sakura root not set. This is fine! Other scripts may require you to set the environment variable first, e.g.:"
    echo "    export SAKURA_SOURCE_DIR=${SAKURA_SOURCE_DIR}"
fi

cd "$SAKURA_SOURCE_DIR"

find . \( -name tarballs -o -name build -o -name local -o -name CMakeFiles -o -name .ccls-cache \) -prune -o \( -name '*.cpp' -o -name '*.c' -o -name '*.h' -o -name 'CMakeLists.txt' -o -name '*.sh'  -o -name '*.s' -o -name '*.patch' -o -name '*.in' -o -name '*.cmake' \) -print > SakuraOS.files
