.set ENOSYS, 4

.global _syscall_entry
.type _syscall_entry, @function

_syscall_entry:
    /* already on stack: ss, sp, flags, cs, ip.
    ; need to push ax, gs, fs, es, ds, -ENOSYS, bp, di, si, dx, cx, and bx*/
    push %eax
    push %gs
    push %fs
    push %es
    push %ds
    push $-ENOSYS
    push %ebp
    push %edi
    push %esi
    push %edx
    push %ecx
    push %ebx
    push %esp
    /*call do_syscall_in_C*/
    add $4, %esp
    pop %ebx
    pop %ecx
    pop %edx
    pop %esi
    pop %edi
    pop %ebp
    pop %eax
    pop %ds
    pop %es
    pop %fs
    pop %gs
    add $4, %esp
    iret
