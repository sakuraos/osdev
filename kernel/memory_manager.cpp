/**
 * @file memory_manager.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-29-12
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <assert.h>
#include <kernel/memory_manager.h>
#include <kernel/multiboot.h>
#include <stdio.h>

#define MEMORY_DEBUG_INFO

namespace Kernel {
namespace Memory {
static memory_manager s_the;

memory_manager::memory_manager() {}

/**
 * @brief Initalises the memory manager with the info provided by
 * multiboot
 *
 * @param mmap_addr The address of the array of mmap_entries provided by
 * multiboot
 * @param mmap_length The length of the array of mmap_entries provided
 * by multiboot
 */
void memory_manager::initalise(void        *mmap_addr,
                               unsigned int mmap_length) {
#ifdef MEMORY_DEBUG_INFO
    printf("mmap_length= %d\n", mmap_length);
    printf("%d entries\n", mmap_length / sizeof(mmap_entry));
#endif
    for (unsigned int i = 0; i < mmap_length; i += sizeof(mmap_entry)) {
        mmap_entry *mmmt = (mmap_entry *)((uintptr_t)mmap_addr + i);
        int         idx  = i / sizeof(mmap_entry);
#ifdef i386
        base_regions[idx].start_high  = (void *)mmmt->base_addr_high;
        base_regions[idx].start_low   = (void *)mmmt->base_addr_low;
        base_regions[idx].length_high = mmmt->len_high;
        base_regions[idx].length_low  = mmmt->len_low;
#endif
        base_regions[idx].type = (mmap_region::types)mmmt->type;
        num_regions            = idx + 1;
#ifdef MEMORY_DEBUG_INFO
        printf("Start Addr: %x%x | Length: %x%x | Size: %x | Type: "
               "%d\n",
               mmmt->base_addr_high, mmmt->base_addr_low,
               mmmt->len_high, mmmt->len_low, mmmt->size, mmmt->type);
#endif
    }
    for (int i = 0; i < num_regions; i++) {
        if (base_regions[i].type == 1 &&
            base_regions[i].start_low != 0) {
            kernel_memory_base =
                (memory_region *)((uintptr_t)base_regions[i].start_low);
            kernel_memory_base->start =
                (void *)((uintptr_t)base_regions[i].start_low);
            kernel_memory_base->length = base_regions[i].length_low;
            kernel_memory_base->type   = memory_region::free;
            base_region_idx            = i;
        }
    }
}

/**
 * @brief Prints the internal state of the memory manager and all
 * regions of memory
 *
 */
void memory_manager::dump_state() {
    for (int i = 0; i < num_regions; i++) {
        printf("Start Addr: %x%x | Length: %x%x | Type: %d\n",
               base_regions[i].start_high, base_regions[i].start_low,
               base_regions[i].length_high, base_regions[i].length_low,
               base_regions[i].type);
    }
    for (uintptr_t ptr = (uintptr_t)kernel_memory_base;
         ptr < (uintptr_t)base_regions[base_region_idx].start_low +
                   base_regions[base_region_idx].length_low;
         ptr += ((memory_region *)ptr)->length) {
        printf("Addr: %p | Region start: %x | Region length: %x | "
               "Region type: %c\n",
               ptr, ((memory_region *)ptr)->start,
               ((memory_region *)ptr)->length,
               ((memory_region *)ptr)->type ? 'u' : 'f');
    }
}

/**
 * @brief Allocate a new region in memory
 *
 * @param size Size of the region in bytes
 * @param region Specifies userspace or kernelspace
 * @return void* Pointer to the new region
 */
void *memory_manager::request_space(size_t size, mem_region region) {
#ifdef MEMORY_DEBUG_INFO
    printf("request_space(%d, %d);\n", size, region);
#endif
    switch (region) {
        // clang-format off
        // I know this has very long lines but it makes it easier for me to read and understand - hab
    case mem_region::Kernel:
        for (uintptr_t ptr = (uintptr_t)kernel_memory_base;
            ptr < (uintptr_t)base_regions[base_region_idx].start_low + base_regions[0].length_low;
            ptr += ((memory_region *)ptr)->length) {
            if (((memory_region *)ptr)->type == memory_region::free && ((memory_region *)ptr)->length > (size + sizeof(memory_region))) {
                    ((memory_region *)ptr)->type = memory_region::used;
                    ((memory_region *)(ptr + size + sizeof(memory_region)))->start = (void*)(ptr + size + sizeof(memory_region));
                    ((memory_region *)(ptr + size + sizeof(memory_region)))->length = ((memory_region *)ptr)->length - (size + sizeof(memory_region));
                    ((memory_region *)(ptr + size + sizeof(memory_region)))->type = memory_region::free;
                    ((memory_region *)ptr)->length = size + sizeof(memory_region);
                    return (void*)(ptr + sizeof(memory_region));
                break;
            }
        }
        break;
        // clang-format on
    case mem_region::Userspace:
        ASSERT_NOT_REACHED;
        break;

    default:
        ASSERT_NOT_REACHED;
        break;
    }
    return (void *)0;
}

/**
 * @brief Unallocate a region
 *
 * @param addr
 * @param region
 */
void memory_manager::release_space(void *addr, mem_region region) {
#ifdef MEMORY_DEBUG_INFO
    printf("release_space(%x, %d);\n", addr, region);
#endif
    switch (region) {
    case mem_region::Kernel: {
        memory_region *ptr =
            (memory_region *)((uintptr_t)addr - sizeof(memory_region));
        ptr->type = memory_region::free;
        if (this->num_frees++ > FREES_BEFORE) {
            collapse_space();
        }
    } break;

    case mem_region::Userspace:
        break;

    default:
        ASSERT_NOT_REACHED;
        break;
    }
}

/**
 * @brief Collapse freed space
 */
void memory_manager::collapse_space() {
    // clang-format off
    for (uintptr_t ptr = (uintptr_t) kernel_memory_base;
         ptr < (uintptr_t) base_regions[base_region_idx].start_low + base_regions[base_region_idx].length_low;
         ptr += ((memory_region *)ptr)->length) {
        if (((memory_region *)ptr)->type == memory_region::free &&
            ((memory_region *)(ptr + (((memory_region *)ptr)->length)))->type == memory_region::free) {
                ((memory_region *)ptr)->length = (((memory_region *)ptr)->length +
                 ((memory_region *)(ptr + (((memory_region *)ptr)->length)))->length);
        }
    }
    // clang-format on
}

/**
 * @brief Returns the instance of memory_manager
 *
 * @return memory_manager& Reference to the memory_manager
 */
memory_manager &memory_manager::the() { return s_the; }

} // namespace Memory
} // namespace Kernel
