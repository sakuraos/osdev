/**
 * @file interrupts.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-07-09
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdint.h>
#include <sys/types.h>

#ifndef KERNEL_INTERRUPTS_H
#define KERNEL_INTERRUPTS_H

namespace Kernel {
namespace Interrupts {

struct interrupt_frame {
    uint32_t eflags;
    uint32_t cs;
    uint32_t eip;
};

__attribute__((interrupt)) void
generic_unused_interrupt(struct interrupt_frame *);

__attribute__((interrupt)) void serial_isr(struct interrupt_frame *);

__attribute__((interrupt)) void ps2_kbd_isr(struct interrupt_frame *);

__attribute__((interrupt)) void pit_isr(struct interrupt_frame *);

__attribute__((interrupt)) void
general_protection_exception(struct interrupt_frame *, size_t);
} // namespace Interrupts
} // namespace Kernel
#endif