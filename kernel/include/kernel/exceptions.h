/**
 * @file exceptions.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2024-01-16
 *
 * @copyright Copyright (c) 2024
 *
 */

#ifndef KERNEL_EXCEPTIONS_H
#define KERNEL_EXCEPTIONS_H

namespace Kernel {
namespace Exceptions {
void init();
}
} // namespace Kernel

#endif