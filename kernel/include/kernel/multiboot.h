/**
 * @file multiboot.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */
#include <stdint.h>

#ifndef KERNEL_MULTIBOOT_H
#define KERNEL_MULTIBOOT_H

typedef struct {
    uint32_t *mod_start;
    uint32_t *mod_end;
    char     *string;
    uint32_t  reserved;
} module_entry;

typedef struct {
    uint32_t size;
    uint32_t base_addr_low;
    uint32_t base_addr_high;
    uint32_t len_low;
    uint32_t len_high;
    uint32_t type;
} mmap_entry;

typedef struct {
    uint32_t size;
    uint8_t  drive_num;
    uint8_t  drive_mode;
    uint16_t drive_cylinders;
    uint8_t  drive_heads;
    uint8_t  drive_sector;
    uint16_t drive_ports[];
} drive_entry;

typedef struct {
    uint16_t version;
    uint16_t cseg;
    uint32_t offset;
    uint16_t cseg16;
    uint16_t dseg;
    uint16_t flags;
} apm_entry;

typedef struct {
    union {
        uint32_t flags;
        struct {
            uint32_t mem                  : 1;
            uint32_t boot_dev             : 1;
            uint32_t cmdline              : 1;
            uint32_t modules              : 1;
            uint32_t aout_symbols         : 1;
            uint32_t elf_symbols          : 1;
            uint32_t mmap_table           : 1;
            uint32_t drives               : 1;
            uint32_t config_table_present : 1;
            uint32_t bootloader           : 1;
            uint32_t apm_table_present    : 1;
            uint32_t vbe_table            : 1;
            uint32_t framebuffer          : 1;
            uint32_t unused               : 19;
        };
    };
    uint32_t      mem_upper;
    uint32_t      mem_lower;
    uint32_t      boot_device;
    char         *cmdline_ptr;
    uint32_t      mods_count;
    module_entry *mods_addr;
    union {
        struct {
            uint32_t tabsize;
            uint32_t strsize;
            uint32_t aout_addr;
            uint32_t reserved;
        };
        struct {
            uint32_t num;
            uint32_t size;
            uint32_t elf_addr;
            uint32_t shndx;
        };
    };
    uint32_t     mmap_length;
    mmap_entry  *mmap_addr;
    uint32_t     drives_length;
    drive_entry *drives_addr;
    uint32_t     config_table;
    char        *boot_loader_name;
    apm_entry   *apm_table;
    uint32_t     vbe_control_info;
    uint32_t     vbe_mmode_info;
    uint32_t     vbe_mode;
    uint32_t     vbe_interface_seg;
    uint32_t     vbe_interface_off;
    uint32_t     vbe_interface_len;
    uint32_t     framebuffer_addr;
    uint32_t     framebuffer_pitch;
    uint32_t     framebuffer_width;
    uint32_t     framebuffer_height;
    uint32_t     framebuffer_bpp;
    uint32_t     framebuffer_type;
    uint8_t      color_info[6];
} multiboot_info;

#endif
