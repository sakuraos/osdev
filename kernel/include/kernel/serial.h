/**
 * @file serial.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-13
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef KERNEL_SERIAL_H
#define KERNEL_SERIAL_H

namespace Kernel {
namespace Serial {

#define COM1 0x3F8
#define COM2 0x2F8
#define COM3 0x3E8
#define COM4 0x2E8

#define DATA_AVAIL_IE   0x01
#define THR_EMPTY_IE    0x02
#define LINE_STATUS_IE  0x04
#define MODEM_STATUS_IE 0x08

#define INT_PEND         0x01
#define DATA_AVAIL_INT   0x04
#define THR_EMPTY_INT    0x02
#define LINE_STATUS_INT  0x05
#define MODEM_STATUS_INT 0x00

int  initalise(bool);
void putchar(char);
void write(char);
bool get_debug(void);
void register_interrupt(void);
void enable_interrupt(unsigned char);

} // namespace Serial
} // namespace Kernel

#endif