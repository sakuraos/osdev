/**
 * @file ps2keyboard.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-08-11
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef KERNEL_PS2KEY_H
#define KERNEL_PS2KEY_H

namespace Kernel {

namespace PS2KBRD {

struct command {
    unsigned char cmd;
    unsigned char data;
};

void initalise();
void send_command(command);

} // namespace PS2KBRD
} // namespace Kernel

#endif