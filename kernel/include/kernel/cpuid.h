/**
 * @file cpuid.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2023-04-13
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef KERNEL_CPUID_H
#define KERNEL_CPUID_H

namespace Kernel {
namespace CPUID {
struct s_cpu_info {
    bool cpuid_supported;
    int  max_supported;
    char vendor_id[13];
};
extern s_cpu_info cpu_info;
struct cpuid_return {
    int eax;
    int ebx;
    int ecx;
    int edx;
};
bool         check_cpuid();
cpuid_return cpuid(int);
void         initalise();
} // namespace CPUID
} // namespace Kernel

#endif
