/**
 * @file io.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-13
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef KERNEL_IO_H
#define KERNEL_IO_H

namespace Kernel {
namespace IO {

__attribute__((no_caller_saved_registers)) unsigned char  in8(int);
__attribute__((no_caller_saved_registers)) unsigned short in16(int);
__attribute__((no_caller_saved_registers)) void           out8(int,
                                                               unsigned char);
__attribute__((no_caller_saved_registers)) void           out16(int,
                                                                unsigned short);

} // namespace IO
} // namespace Kernel

#endif