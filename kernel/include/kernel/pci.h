/**
 * @file pci.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-04-03
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef KERNEL_PCI_H
#define KERNEL_PCI_H

namespace Kernel {
namespace PCI {}
} // namespace Kernel

#endif