/**
 * @file memory_manager.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-29-12
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <hab/noncopyable.h>
#include <sys/types.h>

#ifndef KERNEL_MEMORY_MANAGER_H
#define KERNEL_MEMORY_MANAGER_H

struct mmap_region {
#ifdef i386
    void  *start_low;
    void  *start_high;
    size_t length_low;
    size_t length_high;
#endif
    enum types {
        available,
        reserved,
        acpi_reclaimable,
        nvs,
        badram,
    } type;
};
struct memory_region {
    void  *start;
    size_t length;
    enum types {
        free,
        used,
    } type;
};
namespace Kernel {
namespace Memory {
#define MAX_REGIONS  8
#define FREES_BEFORE 16
class memory_manager {
    // enum class mem_region { Kernel, Userspace };

    HAB_MAKE_NONCOPYABLE(memory_manager);
    HAB_MAKE_NONMOVABLE(memory_manager);

  public:
    enum class mem_region { Kernel, Userspace };

    void                   initalise(void *, unsigned int);
    static memory_manager &the();
    void                   dump_state();
    void                  *request_space(size_t, mem_region);
    void                   release_space(void *, mem_region);
    void                   collapse_space();

  public:
    memory_manager();

  private:
    // void *operator new(size_t) {}

  private:
    memory_region *kernel_memory_base{0};
    int            base_region_idx{0};
    mmap_region    base_regions[MAX_REGIONS];
    int            num_regions{0};
    int            num_frees{0};
};
} // namespace Memory
} // namespace Kernel

#endif
