/**
 * @file pit.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-08-14
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef KERNEL_PIT_H
#define KERNEL_PIT_H

namespace Kernel {
namespace PIT {

enum PORT { CH0 = 0x40, CH1, CH2, MODE_COMMAND };

struct MODE_COMMAND_WORD {
    enum CHANNEL { CH0, CH1, CH2, READ_BACK }                   : 2;
    enum ACCESS_MODE { LATCH_COUNT, LOBYTE, HIBYTE, LO_HIBYTE } : 2;
    enum OP_MODE {
        INT,
        ONE_SHOT,
        RATE,
        SQR_WAVE,
        SW_STROBE,
        HW_STROBE,
        RATE2,
        SQ_WAVE2
    }                              : 3;
    enum BCD_BIN_MODE { BIN, BCD } : 1;
};

void initalise();
int  get_ticks();

} // namespace PIT
} // namespace Kernel

#endif