/**
 * @file bga.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-08-20
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef KERNEL_BGA_H
#define KERNEL_BGA_H

namespace Kernel {
namespace BGA {

#define VBE_DISP_IOPORT_INDEX 0x01CE
#define VBE_DISP_IOPORT_DATA  0x01CF

enum class REGISTER {
    VBE_DISPI_INDEX_ID,
    VBE_DISPI_INDEX_XRES,
    VBE_DISPI_INDEX_YRES,
    VBE_DISPI_INDEX_BPP,
    VBE_DISPI_INDEX_ENABLE,
    VBE_DISPI_INDEX_BANK,
    VBE_DISPI_INDEX_VIRT_WIDTH,
    VBE_DISPI_INDEX_VIRT_HEIGHT,
    VBE_DISPI_INDEX_X_OFFSET,
    VBE_DISPI_INDEX_Y_OFFSET
};

void           initalise();
unsigned short read_register(REGISTER);
void           write_register(REGISTER, unsigned short);
} // namespace BGA
} // namespace Kernel

#endif