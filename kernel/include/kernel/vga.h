/**
 * @file vga.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef KERNEL_VGA_H
#define KERNEL_VGA_H

#include <sys/types.h>

namespace Kernel {
namespace VGA {

#define VID_MEM 0x000b8000

enum vga_color {
    VGA_COLOR_BLACK         = 0,
    VGA_COLOR_BLUE          = 1,
    VGA_COLOR_GREEN         = 2,
    VGA_COLOR_CYAN          = 3,
    VGA_COLOR_RED           = 4,
    VGA_COLOR_MAGENTA       = 5,
    VGA_COLOR_BROWN         = 6,
    VGA_COLOR_LIGHT_GREY    = 7,
    VGA_COLOR_DARK_GREY     = 8,
    VGA_COLOR_LIGHT_BLUE    = 9,
    VGA_COLOR_LIGHT_GREEN   = 10,
    VGA_COLOR_LIGHT_CYAN    = 11,
    VGA_COLOR_LIGHT_RED     = 12,
    VGA_COLOR_LIGHT_MAGENTA = 13,
    VGA_COLOR_LIGHT_BROWN   = 14,
    VGA_COLOR_WHITE         = 15,
};

void terminal_initalise(void);
void terminal_set_color(enum vga_color, enum vga_color);
void terminal_putchar(char);
void terminal_putchar(char, unsigned char);

void  enable_cursor(char, char);
void  disable_cursor();
void  update_cursor(int, int);
short get_cursor_position();

} // namespace VGA
} // namespace Kernel

#endif
