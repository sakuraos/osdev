/**
 * @file gdt.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-26
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdint.h>

#ifndef KERNEL_GDT_H
#define KERNEL_GDT_H

namespace Kernel {
namespace GDT {

#define GDT_SELECTOR_CODE0 0x08
#define GDT_SELECTOR_DATA0 0x10
#define GDT_SELECTOR_CODE3 0x18
#define GDT_SELECTOR_DATA3 0x20

struct [[gnu::packed]] descriptorTablePointer {
    uint16_t limit;
    void    *address;
};

union [[gnu::packed]] descriptor {
    struct {
        uint16_t limit_lo;
        uint16_t base_lo;
        uint8_t  base_hi;
        uint8_t  type            : 4;
        uint8_t  descriptor_type : 1;
        uint8_t  privilege_level : 2;
        uint8_t  segment_present : 1;
        uint8_t  limit_hi        : 4;
        uint8_t                  : 1;
        uint8_t operation_size64 : 1;
        uint8_t operation_size32 : 1;
        uint8_t granularity      : 1;
        uint8_t base_hi2;
    };
    struct {
        uint32_t low;
        uint32_t high;
    };
};

void gdt_init();
void write_raw_gdt_entry(uint16_t, uint32_t, uint32_t);
void write_gdt_entry(uint16_t, descriptor);
void flush_gdt();

} // namespace GDT
} // namespace Kernel

#endif