/**
 * @file acpi.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-04-03
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <stdint.h>

#ifndef KERNEL_ACPI_H
#define KERNEL_ACPI_H

namespace Kernel {
namespace ACPI {

struct [[gnu::packed]] RDSP {
    char     signature[8];
    uint8_t  checksum;
    char     oemid[6];
    uint8_t  revision;
    uint32_t RDST_addr;

    uint32_t length;
    uint64_t XSDT_addr;
    uint8_t  ext_checksum;
    uint8_t  reserved[3];

    void dump_state();
};

class ACPI {

  public:
    void         initalise();
    void         dump_state();
    static ACPI &the();

  private:
    RDSP *find_rsdp();
    RDSP *m_rdsp;
};

} // namespace ACPI
} // namespace Kernel

#endif