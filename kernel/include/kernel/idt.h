/**
 * @file idt.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-30
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <kernel/interrupts.h>
#include <stdint.h>
#include <sys/types.h>

#ifndef KERNEL_IDT_H
#define KERNEL_IDT_H

namespace Kernel {
namespace IDT {

struct [[gnu::packed]] descriptorTablePointer {
    uint16_t limit;
    void    *address;
};

union [[gnu::packed]] descriptor {
    struct {
        uint16_t offset_lo;
        uint16_t segment;
        uint8_t  zero            : 8;
        uint8_t  type            : 4;
        uint8_t                  : 1;
        uint8_t  privilege_level : 2;
        uint8_t  segment_present : 1;
        uint16_t offset_hi;
    };
    struct {
        uint32_t low;
        uint32_t high;
    };
};

void idt_init();
void idt_add_entry(
    uint8_t, uint8_t, uint16_t,
    void (*)(struct Kernel::Interrupts::interrupt_frame *));
void idt_add_entry(
    uint8_t, uint8_t, uint16_t,
    void (*)(struct Kernel::Interrupts::interrupt_frame *, size_t));
void flush_idt();

} // namespace IDT
} // namespace Kernel

#endif