/**
 * @file serial.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-13
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <hab/ringbuffer.h>
#include <kernel/gdt.h>
#include <kernel/idt.h>
#include <kernel/interrupts.h>
#include <kernel/io.h>
#include <kernel/pic.h>
#include <kernel/serial.h>

namespace Kernel {
namespace Serial {

static bool serial_debug;

bool serial_interrupt;
bool serial_interrupt_enabled;

HAB::RingBuffer<char> write_buffer;

/**
 * @brief Initalise the serial port to 57k6 8n1
 *
 * @param debug True if stdout should also go to serial
 * @return int 0 if initalised successfully
 */
int initalise(bool debug) {
    serial_debug = debug;
    IO::out8(COM1 + 1, 0x00);
    IO::out8(COM1 + 3, 0x80); // Div Latch access
    IO::out8(COM1 + 0, 0x02); // divide 115k2 by 2
    IO::out8(COM1 + 1, 0x00);
    IO::out8(COM1 + 3, 0x03); // 8n1
    IO::out8(COM1 + 2, 0x00);
    IO::out8(COM1 + 4, 0x82); // loopback
    IO::out8(COM1 + 0, 0xAE);

    if (IO::in8(COM1 + 0) != 0xAE) { // test if working
        return -1;
    }

    IO::out8(COM1 + 0, 0x00);

    serial_interrupt = false;

    IO::out8(COM1 + 4, 0x01); /// dtr

    return 0;
}

/**
 * @brief Cleaner function to send characters that converts line endings
 *
 * @param ch Character to send
 */
void putchar(char ch) {
    if (ch == '\n')
        write('\r');
    write(ch);
}

/**
 * @brief Function to handle the buffer and the interrupt
 *
 * @param ch Character to send
 */
void write(char ch) {
    if (serial_interrupt) {
        write_buffer.write(ch);
        if (!serial_interrupt_enabled) {
            /*This bit of code was the most annoying thing to get right.
             * I had these two lines setup the other way around which
             * meant that the interrupt was called and sent the
             * character and then was called a second time which
             * disables the interrupt all before the flag was set to
             * indicate to the code that the interrupt was enabled. This
             * meant that the code thoought the interrupt was enaabled
             * andd took that path while the inteerruupt was dissabled
             * in hardware meaning the isr never got called. RIP to my
             * time and sanity. :^)*/
            serial_interrupt_enabled = true;
            IO::out8(COM1 + 1, IO::in8(COM1 + 1) | THR_EMPTY_IE);
        }
    } else {
        while (!(IO::in8(COM1 + 5) & 0x20)) {
        }
        IO::out8(COM1 + 0, ch);
    }
}

/**
 * @brief Get the status of the serial debug
 *
 * @return bool The status of serial debug
 */
bool get_debug(void) { return serial_debug; }

/**
 * @brief Add the serial interrupt to the IDT
 *
 */
void register_interrupt() {
    Kernel::IDT::idt_add_entry(0b10001110, 0x24, GDT_SELECTOR_CODE0,
                               Kernel::Interrupts::serial_isr);
}

/**
 * @brief Enable the serial interrupt
 *
 * @param char Port to enable
 */
void enable_interrupt(unsigned char) {
    serial_interrupt = true;
    Kernel::PIC::enable(4);
}

} // namespace Serial
} // namespace Kernel
