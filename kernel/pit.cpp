/**
 * @file pit.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-08-14
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <kernel/gdt.h>
#include <kernel/idt.h>
#include <kernel/interrupts.h>
#include <kernel/io.h>
#include <kernel/pic.h>
#include <kernel/pit.h>

namespace Kernel {
namespace PIT {

unsigned long long millis;
unsigned char      hundreds;

/**
 * @brief Initalise the PIT to trigger an interrupt 10000 times per
 * second
 *
 */
void initalise() {
    IO::out8(PORT::MODE_COMMAND, 0b00110000);
    IO::out8(PORT::CH0, 119);
    IO::out8(PORT::CH0, 0);
    Kernel::IDT::idt_add_entry(0b10001110, 0x20, GDT_SELECTOR_CODE0,
                               Kernel::Interrupts::pit_isr);
    PIC::enable(0);
    int now = millis;
    while (now == (int)millis)
        ;
}

/**
 * @brief Get the number of milliseconds elapsed since the timer was
 * started
 *
 * @return int Number of milliseconds elapsed
 */
int get_ticks() { return (int)millis; }

} // namespace PIT
} // namespace Kernel