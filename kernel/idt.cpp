/**
 * @file idt.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-30
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <assert.h>
#include <kernel/gdt.h>
#include <kernel/idt.h>
#include <kernel/interrupts.h>
#include <sys/types.h>

namespace Kernel {
namespace IDT {

descriptor             idt_list[256];
descriptorTablePointer idtr;

/**
 * @brief Initalises the IDT with 256 entries to an empty function
 *
 */
void idt_init() {
    for (int i = 0; i < 255; i++) {
        idt_add_entry(0b10001110, i, GDT_SELECTOR_CODE0,
                      Kernel::Interrupts::generic_unused_interrupt);
    }
    flush_idt();
    asm volatile("sti");
}

/**
 * @brief Adds an entry into the IDT
 *
 * @param flags
 * @param int_num Interrupt number to set
 * @param segment
 * @param func Pointer to a function returning void and taking a pointer
 * to the interrupt frame
 */
void idt_add_entry(
    uint8_t flags, uint8_t int_num, uint16_t segment,
    void (*func)(struct Kernel::Interrupts::interrupt_frame *)) {
    idt_list[int_num].segment   = segment;
    idt_list[int_num].offset_lo = ((uint32_t)func) & 0x0000ffff;
    idt_list[int_num].offset_hi = (((uint32_t)func) & 0xffff0000) >> 16;
    idt_list[int_num].high &= 0xffff0000;
    idt_list[int_num].high |= (uint16_t)flags << 8;
}

/**
 * @brief Adds an entry into the IDT
 *
 * @param flags
 * @param int_num Interrupt number to set
 * @param segment
 * @param func Pointer to a function returning void and taking a pointer
 * to the interrupt frame and an error code
 */
void idt_add_entry(
    uint8_t flags, uint8_t int_num, uint16_t segment,
    void (*func)(struct Kernel::Interrupts::interrupt_frame *,
                 size_t)) {
    idt_list[int_num].segment   = segment;
    idt_list[int_num].offset_lo = ((uint32_t)func) & 0x0000ffff;
    idt_list[int_num].offset_hi = (((uint32_t)func) & 0xffff0000) >> 16;
    idt_list[int_num].high &= 0xffff0000;
    idt_list[int_num].high |= (uint16_t)flags << 8;
}

/**
 * @brief Reloads the IDTR so the prrocessor will recognise the IDT
 *
 */
void flush_idt() {
    idtr.address = idt_list;
    idtr.limit   = (256 * 8) - 1;
    asm volatile("lidt %0" ::"m"(idtr));
}

} // namespace IDT
} // namespace Kernel