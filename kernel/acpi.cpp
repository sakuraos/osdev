/**
 * @file acpi.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-04-03
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <kernel/acpi.h>
#include <stdio.h>

namespace Kernel {
namespace ACPI {

void RDSP::dump_state() {
    printf("rdsp: %p\n", this);
    printf("signature: %c%c%c%c%c%c%c%c\n", signature[0], signature[1],
           signature[2], signature[3], signature[4], signature[5],
           signature[6], signature[7]);
    printf("checksum: %d\n", checksum);
    printf("oemid: %c%c%c%c%c%c\n", oemid[0], oemid[1], oemid[2],
           oemid[3], oemid[4], oemid[5]);
    printf("revision: %d\n", revision);
    printf("RDST_addr: %p\n", RDST_addr);

    printf("length: %d\n", length);
    printf("XSDT_addr: %p\n", XSDT_addr);
    printf("ext_checksum: %d\n", ext_checksum);
}

static ACPI s_the;

ACPI &ACPI::the() { return s_the; };

void ACPI::initalise() { m_rdsp = find_rsdp(); }

/**
 * @brief
 *
 * @return RDSP* Pointer to the RSDP in memory or zero if not found
 */
RDSP *ACPI::find_rsdp() {
    for (char *ptr = (char *)0xc00e0000; ptr < (char *)0xc0100000;
         ptr += 16) {
        if (ptr[0] == 'R' && ptr[1] == 'S' && ptr[2] == 'D' &&
            ptr[3] == ' ' && ptr[4] == 'P' && ptr[5] == 'T' &&
            ptr[6] == 'R' && ptr[7] == ' ')
            return (RDSP *)ptr;
    }
    return 0;
}

/**
 * @brief Prints the internal state of the ACPI
 *
 */
void ACPI::dump_state() { m_rdsp->dump_state(); }

} // namespace ACPI
} // namespace Kernel