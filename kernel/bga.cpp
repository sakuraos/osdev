/**
 * @file bga.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-08-20
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <kernel/bga.h>
#include <kernel/io.h>
#include <stdio.h>

namespace Kernel {
namespace BGA {
void initalise() {
    unsigned short vbe_version =
        read_register(REGISTER::VBE_DISPI_INDEX_ID);
    if ((vbe_version & 0xfff0) != 0xb0c0) {
        fprintf(stderr, "BGA not supported\n");
        return;
    }
}

unsigned short read_register(REGISTER reg) {
    IO::out16(VBE_DISP_IOPORT_INDEX, (short)reg);
    return IO::in16(VBE_DISP_IOPORT_DATA);
}

void write_register(REGISTER reg, unsigned short val) {
    IO::out16(VBE_DISP_IOPORT_INDEX, (short)reg);
    IO::out16(VBE_DISP_IOPORT_DATA, val);
}
} // namespace BGA
} // namespace Kernel