/**
 * @file ps2keyboard.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-08-11
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <hab/ringbuffer.h>
#include <kernel/gdt.h>
#include <kernel/idt.h>
#include <kernel/interrupts.h>
#include <kernel/io.h>
#include <kernel/pic.h>
#include <kernel/pit.h>
#include <kernel/ps2keyboard.h>

namespace Kernel {

namespace PS2KBRD {

HAB::RingBuffer<command> command_buffer;
HAB::RingBuffer<char>    read_buffer;
char                     key_status[104];

void initalise() {
    Kernel::IDT::idt_add_entry(0b10001110, 0x24, GDT_SELECTOR_CODE0,
                               Kernel::Interrupts::ps2_kbd_isr);
    Kernel::PIC::enable(1);
    Kernel::IO::in8(0x60);
    send_command({0xff, 0x00});
}

void send_command(command cmd) { command_buffer.write(cmd); }

char get_char() {
    while (read_buffer.is_empty())
        ;
    return read_buffer.read();
}

} // namespace PS2KBRD
} // namespace Kernel