/**
 * @file pci.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-04-03
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <kernel/pci.h>

namespace Kernel {
namespace PCI {}
} // namespace Kernel