/**
 * @file kmain.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */
#include <assert.h>
#include <kernel/acpi.h>
#include <kernel/bga.h>
#include <kernel/cpuid.h>
#include <kernel/exceptions.h>
#include <kernel/gdt.h>
#include <kernel/idt.h>
#include <kernel/memory_manager.h>
#include <kernel/multiboot.h>
#include <kernel/pci.h>
#include <kernel/pic.h>
#include <kernel/pit.h>
#include <kernel/ps2keyboard.h>
#include <kernel/serial.h>
#include <kernel/vga.h>
#include <stdio.h>

#define MULTIBOOT_DEBUG

multiboot_info *multiboot_info_ptr;

extern "C" void kernel_main() {

    Kernel::VGA::terminal_initalise();
    if (!Kernel::Serial::initalise(
            true)) // todo fix once strstr is implemented
        fprintf(stderr, "serial failed to initalise\n");

#ifdef MULTIBOOT_DEBUG
    if (multiboot_info_ptr->mem) {
        printf("memory info provided\n");
        printf("mem_lower= %x\n", multiboot_info_ptr->mem_lower);
        printf("mem_upper= %x\n", multiboot_info_ptr->mem_upper);
        printf("mmap_length= %d\n", multiboot_info_ptr->mmap_length);
        for (unsigned int i = 0; i < multiboot_info_ptr->mmap_length;
             i += sizeof(mmap_entry)) {
            mmap_entry *mmmt =
                (mmap_entry *)((char *)multiboot_info_ptr->mmap_addr +
                               i);
            printf("Start Addr: %x%x | Length: %x%x | Size: %x | Type: "
                   "%d\n",
                   mmmt->base_addr_high, mmmt->base_addr_low,
                   mmmt->len_high, mmmt->len_low, mmmt->size,
                   mmmt->type);
        }
    }

    if (multiboot_info_ptr->cmdline) {
        printf("command line param addr: %p\n",
               multiboot_info_ptr->cmdline_ptr);
        printf("command line params passed: %s\n",
               multiboot_info_ptr->cmdline_ptr);
    }
    if (multiboot_info_ptr->vbe_table)
        printf("vbe info provided\n");
    if (multiboot_info_ptr->framebuffer)
        printf("framebuffer provided\n");
    if (multiboot_info_ptr->bootloader)
        printf("loaded by: %s\n", multiboot_info_ptr->boot_loader_name);
    if (multiboot_info_ptr->apm_table_present)
        printf("apm_table present\n");
    //*((char *)0) = 0; // Hack to force crash for debuging
#endif

    printf("Initalising cpu info\n");
    Kernel::CPUID::initalise();
    printf("CPUID %s\n", Kernel::CPUID::cpu_info.cpuid_supported
                             ? "supported"
                             : "unsupported");
    if (Kernel::CPUID::cpu_info.cpuid_supported) {
        printf("Max supported: %x\n",
               Kernel::CPUID::cpu_info.max_supported);
        printf("Vendor ID: %s\n", Kernel::CPUID::cpu_info.vendor_id);
        Kernel::CPUID::cpuid_return ret = Kernel::CPUID::cpuid(1);
        printf("eax=%x\nebx=%x\necx=%x\nedx=%x\n", ret.eax, ret.ebx,
               ret.ecx, ret.edx);
    }

    printf("Initalising GDTs\n");
    Kernel::GDT::gdt_init();

    printf("Initalising PIC\n");
    Kernel::PIC::init(0x20, 0x28);

    printf("Initialising IDTs\n");
    Kernel::IDT::idt_init();

    printf("Initalising exception handlers");
    Kernel::Exceptions::init();

    printf("Initalising memory manager\n");
    Kernel::Memory::memory_manager::the().initalise(
        multiboot_info_ptr->mmap_addr, multiboot_info_ptr->mmap_length);

    Kernel::Memory::memory_manager::the().dump_state();

    auto spc = Kernel::Memory::memory_manager::the().request_space(
        10, Kernel::Memory::memory_manager::mem_region::Kernel);

    Kernel::Memory::memory_manager::the().dump_state();

    Kernel::Memory::memory_manager::the().release_space(
        spc, Kernel::Memory::memory_manager::mem_region::Kernel);

    Kernel::Memory::memory_manager::the().dump_state();

    Kernel::Memory::memory_manager::the().collapse_space();

    Kernel::Memory::memory_manager::the().dump_state();

    printf("Initalising serial interrupt\n");
    Kernel::Serial::register_interrupt();
    // Kernel::Serial::enable_interrupt(1);

    printf("Initalising PIT\n");
    Kernel::PIT::initalise();

    printf("Initalising PS/2\n");
    // Kernel::PS2KBRD::initalise();

    printf("Welcome to sakuraOS!\nLoading in progress...\n");

    printf("Initalising ACPI...\n");
    Kernel::ACPI::ACPI::the().initalise();
    Kernel::ACPI::ACPI::the().dump_state();

    printf("Enumerating PCI bus...\n");

    // Kernel::BGA::initalise();

    // asm volatile("int $0x24");

    // while (1)
    ;
}
