/**
 * @file vga.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */
#include <kernel/io.h>
#include <kernel/vga.h>

namespace Kernel {
namespace VGA {

static const size_t VGA_WIDTH  = 80;
static const size_t VGA_HEIGHT = 25;

size_t          terminal_row;
size_t          terminal_column;
unsigned char   terminal_color;
unsigned short *terminal_buffer;

/**
 * @brief Generate the correctly structured value for the vga color
 *
 * @param fg Foreground color
 * @param bg Background color
 * @return unsigned char Packed vga color
 */
static inline unsigned char vga_entry_color(enum vga_color fg,
                                            enum vga_color bg) {
    return fg | bg << 4;
}

/**
 * @brief Generate the correctly structured value for the vga character
 *
 * @param uc Character to display
 * @param color Color to use
 * @return unsigned short Packed vga character
 */
static inline unsigned short vga_entry(unsigned char uc,
                                       unsigned char color) {
    return (unsigned short)uc | (unsigned short)color << 8;
}

/**
 * @brief Initalise the vga text mode display
 *
 */
void terminal_initalise(void) {
    terminal_row    = 0;
    terminal_column = 0;
    terminal_color =
        vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLUE);
    terminal_buffer = (unsigned short *)0xc00b8000;
    for (size_t y = 0; y < VGA_HEIGHT; y++) {
        for (size_t x = 0; x < VGA_WIDTH; x++) {
            terminal_buffer[y * VGA_WIDTH + x] =
                vga_entry(' ', terminal_color);
        }
    }
    disable_cursor();
    enable_cursor(13, 15);
    update_cursor(0, 0);
}

/**
 * @brief Set the default color for the display
 *
 * @param fg Foreground color
 * @param bg Background color
 */
void terminal_set_color(enum vga_color fg, enum vga_color bg) {
    terminal_color = vga_entry_color(fg, bg);
}

/**
 * @brief Scroll the terminal by one line
 *
 */
void terminal_scroll() {
    for (size_t y = 1; y < VGA_HEIGHT; y++) {
        for (size_t x = 0; x < VGA_WIDTH; x++) {
            terminal_buffer[(y - 1) * VGA_WIDTH + x] =
                terminal_buffer[y * VGA_WIDTH + x];
        }
    }
    terminal_column = 0;
    terminal_row    = terminal_row - 1;
    for (size_t x = 0; x < VGA_WIDTH; x++) {
        terminal_buffer[(VGA_HEIGHT - 1) * VGA_WIDTH + x] =
            vga_entry(' ', terminal_color);
    }
}

/**
 * @brief Put a character with the default color
 *
 * @param ch Character to display
 */
void terminal_putchar(char ch) { terminal_putchar(ch, terminal_color); }

/**
 * @brief Put a character with a specific color
 *
 * @param ch Character to display
 * @param color Color to use
 */
void terminal_putchar(char ch, unsigned char color) {
    if (ch != '\n') {
        terminal_buffer[terminal_row * VGA_WIDTH + terminal_column] =
            vga_entry(ch, color);
        if (++terminal_column == VGA_WIDTH) {
            terminal_column = 0;
            if (++terminal_row == VGA_HEIGHT)
                terminal_scroll();
        }
    } else {
        terminal_column = 0;
        if (++terminal_row == VGA_HEIGHT)
            terminal_scroll();
    }
    update_cursor(terminal_column, terminal_row);
}

/**
 * @brief Enable the text mode cursor
 *
 * @param start Scanline to start on (0-15)
 * @param end Scanline to end on
 */
void enable_cursor(char start, char end) {
    IO::out8(0x3d4, 0x0a);
    IO::out8(0x3d5, (IO::in8(0x3d5) & 0xc0) | start);

    IO::out8(0x3d4, 0x0B);
    IO::out8(0x3d5, (IO::in8(0x3d5) & 0xe0) | end);
}

/**
 * @brief Disable the text mode cursor
 *
 */
void disable_cursor() {
    IO::out8(0x3d4, 0x0a);
    IO::out8(0x3d5, 0x20);
}

/**
 * @brief Move the teext mode cursor
 *
 * @param x Column to move to
 * @param y Row to move to
 */
void update_cursor(int x, int y) {
    short pos = y * VGA_WIDTH + x;

    IO::out8(0x3d4, 0x0f);
    IO::out8(0x3d5, (pos & 0xff));
    IO::out8(0x3d4, 0x0e);
    IO::out8(0x3d5, ((pos >> 8) & 0xff));
}

/**
 * @brief Get the cursor position
 *
 * @return short Y * Width + X
 */
short get_cursor_position() {
    short pos = 0;

    IO::out8(0x3d4, 0x0f);
    pos |= IO::in8(0x3d5);
    IO::out8(0x3d5, 0x0e);
    pos |= ((short)IO::in8(0x3d5) << 8);

    return pos;
}
} // namespace VGA
} // namespace Kernel
