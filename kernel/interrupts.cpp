/**
 * @file interrupts.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-07-09
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <assert.h>
#include <hab/ringbuffer.h>
#include <kernel/interrupts.h>
#include <kernel/io.h>
#include <kernel/pic.h>
#include <kernel/pit.h>
#include <kernel/ps2keyboard.h>
#include <kernel/serial.h>

namespace Kernel {
namespace Serial {
extern bool                  serial_interrupt_enabled;
extern HAB::RingBuffer<char> write_buffer;
} // namespace Serial

namespace PS2KBRD {
extern HAB::RingBuffer<command> command_buffer;
extern HAB::RingBuffer<char>    read_buffer;
} // namespace PS2KBRD

namespace PIT {
extern unsigned long long millis;
extern unsigned char      hundreds;
} // namespace PIT
namespace Interrupts {

__attribute__((interrupt)) void
generic_unused_interrupt(struct interrupt_frame *) {
    ASSERT_NOT_REACHED;
}

__attribute__((interrupt)) void serial_isr(struct interrupt_frame *) {
    uint8_t iir = IO::in8(COM1 + 2);
    if ((iir & 0b00000001) == 0) {
        if (Kernel::Serial::write_buffer.is_empty()) {
            IO::out8(COM1 + 1, IO::in8(COM1 + 1) & ~THR_EMPTY_IE);
            Kernel::Serial::serial_interrupt_enabled = false;
        } else {
            IO::out8(COM1 + 0, Kernel::Serial::write_buffer.read());
        }
        Kernel::PIC::eoi(4);
    }
}

__attribute__((interrupt)) void ps2_kbd_isr(struct interrupt_frame *) {
    ASSERT_NOT_REACHED;
}

__attribute__((interrupt)) void pit_isr(struct interrupt_frame *) {
    Kernel::PIT::hundreds = (Kernel::PIT::hundreds + 1) % 100;
    if (Kernel::PIT::hundreds == 0)
        Kernel::PIT::millis += 1;
    IO::out8(Kernel::PIT::PORT::CH0, 119);
    IO::out8(Kernel::PIT::PORT::CH0, 0);
    Kernel::PIC::eoi(0);
    //*((char *)0) = 0; // Hack to force crash for debuging
}

__attribute__((interrupt)) void
general_protection_exception(struct interrupt_frame *f, size_t err) {
    asm volatile(R"(cli
                 1: hlt
                 jmp 1b)");
}
} // namespace Interrupts
} // namespace Kernel
