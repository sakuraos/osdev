/**
 * @file gdt.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-26
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <assert.h>
#include <kernel/gdt.h>

namespace Kernel {
namespace GDT {

descriptor             gdt_list[256];
descriptorTablePointer gdtr;
uint32_t               gdt_length;

/**
 * @brief Initalises the GDT
 *
 */
void gdt_init() {
    write_raw_gdt_entry(0x0000, 0x00000000, 0x00000000); // null entry
    write_raw_gdt_entry(GDT_SELECTOR_CODE0, 0x0000ffff,
                        0x00cf9a00); // code0
    write_raw_gdt_entry(GDT_SELECTOR_DATA0, 0x0000ffff,
                        0x00cf9200); // data0
    write_raw_gdt_entry(GDT_SELECTOR_CODE3, 0x0000ffff,
                        0x00cffa00); // code3
    write_raw_gdt_entry(GDT_SELECTOR_DATA3, 0x0000ffff,
                        0x00cff200); // data3
    flush_gdt();
}

/**
 * @brief Adds or changes an entry in the GDT
 *
 * @param selector Entry to set
 * @param desc GDT descriptor to write
 */
void write_gdt_entry(uint16_t selector, descriptor desc) {
    write_raw_gdt_entry(selector, desc.low, desc.high);
}

/**
 * @brief Manipulates the raw data assigned to the GDT
 * Should use write_gdt_entry over this
 *
 * @param selector Entry to set
 * @param low First 4 bytes
 * @param high Second 4 bytes
 */
void write_raw_gdt_entry(uint16_t selector, uint32_t low,
                         uint32_t high) {
    uint16_t i               = (selector & 0xfffc) >> 3;
    uint32_t prev_gdt_length = gdt_length;

    if (i >= gdt_length) {
        gdt_length = i + 1;
        ASSERT(gdt_length <= sizeof(gdt_list) / sizeof(gdt_list[0]));
        gdtr.limit = (gdt_length + 1) * 8 - 1;
    }
    gdt_list[i].low  = low;
    gdt_list[i].high = high;

    // clear selectors we may have skipped
    while (i < prev_gdt_length) {
        gdt_list[i].low  = 0;
        gdt_list[i].high = 0;
        i++;
    }
}

/**
 * @brief Reloads the GDTR to make the processor recognise the new GDT
 *
 */
void flush_gdt() {
    gdtr.address = gdt_list;
    gdtr.limit   = (gdt_length * 8) - 1;
    asm volatile("lgdt %0" ::"m"(gdtr) : "memory");
    // this just feels wrong. It works but it feels wrong
    // this fixes an issue caused by grub where cs points to the wrong
    // segment. grub loads us to code segment 0x10 but our gdt is based
    // on being in code segment 0x08. we fix this by faking a call stack
    // to return from since returns are the only easy way to change the
    // cs register.
    asm volatile(R"(push $0x08
                    push $1f
                    retf
                    1: nop)");
}

} // namespace GDT
} // namespace Kernel
