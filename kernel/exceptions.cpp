/**
 * @file exceptions.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2024-01-16
 *
 * @copyright Copyright (c) 2024
 *
 */

#include <kernel/exceptions.h>
#include <kernel/gdt.h>
#include <kernel/idt.h>
#include <kernel/interrupts.h>

namespace Kernel {
namespace Exceptions {
void init() {
    Kernel::IDT::idt_add_entry(
        0b10001110, 0x0d, GDT_SELECTOR_CODE0,
        Kernel::Interrupts::general_protection_exception);
}
} // namespace Exceptions
} // namespace Kernel