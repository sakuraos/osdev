/**
 * @file pic.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-07-10
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <kernel/io.h>
#include <kernel/pic.h>

namespace Kernel {
namespace PIC {

/**
 * @brief Initalise the pics to a specific interrupt offset
 *
 * @param primary_offset Offset for the primary PIC
 * @param secondary_offset Offset for the secondary PIC
 */
void init(int primary_offset, int secondary_offset) {
    IO::out8(PIC_PRIMARY_CMD, ICW1_INIT | ICW1_ICW4);
    IO::out8(PIC_SECONDARY_CMD, ICW1_INIT | ICW1_ICW4);
    IO::out8(PIC_PRIMARY_DAT, primary_offset);
    IO::out8(PIC_SECONDARY_DAT, secondary_offset);
    IO::out8(PIC_PRIMARY_DAT, 4);
    IO::out8(PIC_SECONDARY_DAT, 2);
    IO::out8(PIC_PRIMARY_DAT, ICW4_8086);
    IO::out8(PIC_SECONDARY_DAT, ICW4_8086);
    IO::out8(PIC_PRIMARY_DAT, ~(1 << 2));
    IO::out8(PIC_SECONDARY_DAT, 0xff);
}

/**
 * @brief Enable an interrupt on the PICs
 *
 * @param irq Interrupt number to enable
 */
void enable(unsigned char irq) {
    if (irq < 8) {
        IO::out8(PIC_PRIMARY_DAT,
                 IO::in8(PIC_PRIMARY_DAT) & ~(1 << irq));
    } else {
        IO::out8(PIC_SECONDARY_DAT,
                 IO::in8(PIC_SECONDARY_DAT) & ~(1 << (irq - 8)));
    }
}

/**
 * @brief Disable an interruppt on the PICs
 *
 * @param irq Interrupt number to disable
 */
void disable(unsigned char irq) {
    if (irq < 8) {
        IO::out8(PIC_PRIMARY_DAT,
                 IO::in8(PIC_PRIMARY_DAT) | (1 << irq));
    } else {
        IO::out8(PIC_SECONDARY_DAT,
                 IO::in8(PIC_SECONDARY_DAT) | (1 << (irq - 8)));
    }
}

/**
 * @brief Notify the PICs that the interrupt has been handled
 *
 * @param irq Interrupt that has been handled
 */
__attribute__((no_caller_saved_registers)) void eoi(unsigned char irq) {
    if (irq >= 8)
        IO::out8(PIC_SECONDARY_CMD, PIC_EOI);

    IO::out8(PIC_PRIMARY_CMD, PIC_EOI);
}

} // namespace PIC
} // namespace Kernel