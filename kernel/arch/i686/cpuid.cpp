/**
 * @file cpuid.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2023-04-13
 *
 * @copyright Copyright (c) 2023
 *
 */

#include <kernel/cpuid.h>

namespace Kernel {
namespace CPUID {
s_cpu_info cpu_info;
bool       check_cpuid() {
    int ret;
    asm volatile("pushf\n"
                       "pushf\n"
                       "xorl $0x00200000,(%%esp)\n"
                       "popf\n"
                       "pushf\n"
                       "pop %0\n"
                       "xorl (%%esp),%0\n"
                       "popf\n"
                       "andl $0x00200000,%0"
                 : "=r"(ret)
                 :
                 : "eax", "memory");
    return ret;
}

cpuid_return cpuid(int level) {
    cpuid_return ret;
    asm volatile("mov %4, %%eax\n"
                 "cpuid\n"
                 "mov %%eax,%0\n"
                 "mov %%ebx,%1\n"
                 "mov %%ecx,%2\n"
                 "mov %%edx,%3\n"
                 : "=rm"(ret.eax), "=rm"(ret.ebx), "=rm"(ret.ecx),
                   "=rm"(ret.edx)
                 : "rm"(level)
                 : "eax", "ebx", "ecx", "edx", "memory");
    return ret;
}
void initalise() {
    cpu_info.cpuid_supported = check_cpuid() ? true : false;
    cpuid_return ret_val     = cpuid(0);
    cpu_info.max_supported   = ret_val.eax;
    cpu_info.vendor_id[0]    = ret_val.ebx & 0xff;
    cpu_info.vendor_id[1]    = (ret_val.ebx >> 8) & 0xff;
    cpu_info.vendor_id[2]    = (ret_val.ebx >> 16) & 0xff;
    cpu_info.vendor_id[3]    = (ret_val.ebx >> 24) & 0xff;
    cpu_info.vendor_id[4]    = ret_val.edx & 0xff;
    cpu_info.vendor_id[5]    = (ret_val.edx >> 8) & 0xff;
    cpu_info.vendor_id[6]    = (ret_val.edx >> 16) & 0xff;
    cpu_info.vendor_id[7]    = (ret_val.edx >> 24) & 0xff;
    cpu_info.vendor_id[8]    = ret_val.ecx & 0xff;
    cpu_info.vendor_id[9]    = (ret_val.ecx >> 8) & 0xff;
    cpu_info.vendor_id[10]   = (ret_val.ecx >> 16) & 0xff;
    cpu_info.vendor_id[11]   = (ret_val.ecx >> 24) & 0xff;
    cpu_info.vendor_id[12]   = 0;
}

} // namespace CPUID
} // namespace Kernel
