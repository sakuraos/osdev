.set ALIGN, 1<<0
.set MEMINFO, 1<<1
.set VIDMODE, 1<<2
.set FLAGS, ALIGN | MEMINFO /*| VIDMODE*/
.set MAGIC, 0x1BADB002
.set CHECKSUM, -(MAGIC + FLAGS)

.section .multiboot
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM

.long 0
.long 0
.long 0
.long 0
.long 0

.long 0
.long 1024
.long 768
.long 32

.section .stack, "aw", @nobits
.align 16
stack_bottom:
.skip 16384
stack_top:

.section .page_tables, "aw", @nobits
.align 4096
.global boot_pdpt
boot_pdpt:
.skip 4096
.global boot_pd0
boot_pd0:
.skip 4096
.global boot_pd3
boot_pd3:
.skip 4096
.global boot_pd0_pt0
boot_pd0_pt0:
.skip 4096 * 4
.global boot_pd3_pts
boot_pd3_pts:
.skip 4096 * 16
.global boot_pd3_pt1023
boot_pd3_pt1023:
.skip 4096

/*
    construct the following (32-bit PAE) page table layout:
pdpt
    0: boot_pd0 (0-1GB)
    1: n/a      (1-2GB)
    2: n/a      (2-3GB)
    3: boot_pd3 (3-4GB)

boot_pd0 : 512 pde's
    0: boot_pd0_pt0 (0-2MB) (id 512 4KB pages)

boot_pd3 : 512 pde's
    0: boot_pd3_pts[0] (3072-3074MB) (pseudo 512 4KB pages)
    1: boot_pd3_pts[1] (3074-3076MB) (pseudo 512 4KB pages)
    2: boot_pd3_pts[2] (3076-3078MB) (pseudo 512 4KB pages)
    3: boot_pd3_pts[3] (3078-3080MB) (pseudo 512 4KB pages)
    4: boot_pd3_pts[4] (3080-3082MB) (pseudo 512 4KB pages)
    5: boot_pd3_pts[5] (3082-3084MB) (pseudo 512 4KB pages)
    6: boot_pd3_pts[6] (3084-3086MB) (pseudo 512 4KB pages)
    7: boot_pd3_pts[7] (3086-3088MB) (pseudo 512 4KB pages)
    
    8: boot_pd3_pts[8] (3088-3090MB) (pseudo 512 4KB pages)
    9: boot_pd3_pts[9] (3090-3076MB) (pseudo 512 4KB pages)
    10: boot_pd3_pts[10] (3092-3094MB) (pseudo 512 4KB pages)
    11: boot_pd3_pts[11] (3094-3096MB) (pseudo 512 4KB pages)
    12: boot_pd3_pts[12] (3096-3098MB) (pseudo 512 4KB pages)
    13: boot_pd3_pts[13] (3098-3100MB) (pseudo 512 4KB pages)
    14: boot_pd3_pts[14] (3100-3102MB) (pseudo 512 4KB pages)
    15: boot_pd3_pts[15] (3102-3104MB) (pseudo 512 4KB pages)
    
    16: boot_pd3_pt1023 (4094-4096MB) (for page table mappings)
the 17 page tables each contain 512 pte's that map individual 4KB pages
*/

.section .boot_text, "ax"
.global _start
.type _start, @function

.extern multiboot_info_ptr
.type multiboot_info_ptr, @object

_start:
cmpl $0x2BADB002, %eax /*verify we were loaded by a multiboot compliant loader*/
jne halt /*bail early if not to simplify things*/

/*movl %ebx, multiboot_info_ptr 
mov $stack_top, %esp
call kernel_main
cli
jmp halt
/*temp to ensure nothing is breaking, this will get broken when paging is enabled*/

cli
cld

/* clear pdpt */
movl $(boot_pdpt - 0xc0000000), %edi
movl $1024, %ecx
xorl %eax, %eax
rep stosl

/* set up pdpt[0] and pdpt[3] */
movl $(boot_pdpt - 0xc0000000), %edi
movl $((boot_pd0 - 0xc0000000) + 1), 0(%edi)
movl $((boot_pd3 - 0xc0000000) + 1), 24(%edi)

/* clear pd0's pt's */
movl $(boot_pd0_pt0 - 0xc0000000), %edi
movl $(1024 * 4), %ecx
xorl %eax, %eax
rep stosl

/* clear pd3's pt's */
movl $(boot_pd3_pts - 0xc0000000), %edi
movl $(1024 * 17), %ecx
xorl %eax, %eax
rep stosl

/* add boot_pd0_pt0 to boot_pd0 */
movl $(boot_pd0 - 0xc0000000), %edi
movl $(boot_pd0_pt0 - 0xc0000000), %eax
movl %eax, 0(%edi)
/* R/W + Present */
orl $0x3, 0(%edi)

/* add boot_pd3_pts to boot_pd3 */
movl $16, %ecx
movl $(boot_pd3 - 0xc0000000), %edi
movl $(boot_pd3_pts - 0xc0000000), %eax

1:
movl %eax, 0(%edi)
/* R/W + Present */
orl $0x3, 0(%edi)
addl $8, %edi
addl $4096, %eax
loop 1b

/* identity map the 0 to 2MB range */
movl $512, %ecx
movl $(boot_pd0_pt0 - 0xc0000000), %edi
xorl %eax, %eax

1:
movl %eax, 0(%edi)
/* R/W + Present */
orl $0x3, 0(%edi)
addl $8, %edi
addl $4096, %eax
loop 1b

/* pseudo identity map the 3072-3102MB range */
movl $(512 * 16), %ecx
movl $(boot_pd3_pts - 0xc0000000), %edi
xorl %eax, %eax

1:
movl %eax, 0(%edi)
/* R/W + Present */
orl $0x3, 0(%edi)
addl $8, %edi
addl $4096, %eax
loop 1b

/* create an empty page table for the top 2MB at the 4GB mark */
movl $(boot_pd3 - 0xc0000000), %edi
movl $(boot_pd3_pt1023 - 0xc0000000), 4088(%edi)
orl $0x3, 4088(%edi)
movl $0, 4092(%edi)

/* point CR3 to PDPT */
movl $(boot_pdpt - 0xc0000000), %eax
movl %eax, %cr3

/* enable PAE + PSE */
movl %cr4, %eax
orl $0x70, %eax
movl %eax, %cr4

/* enable PG */
movl %cr0, %eax
orl $0x80000000, %eax
movl %eax, %cr0

nop
nop
nop
nop
nop

/*setup stack*/
movl $stack_top, %esp
and $-16, %esp

/* jmp to an address above the 3GB mark */
movl $1f,%eax
jmp *%eax
1:
movl %cr3, %eax
movl %eax, %cr3

/* unmap the 0-1MB range, which isn't used after jmp-ing up here */
movl $256, %ecx
movl $(boot_pd0_pt0 - 0xc0000000), %edi
xorl %eax, %eax

1:
movl %eax, 0(%edi)
addl $8, %edi
loop 1b

add $0xc0000000, %ebx
movl %ebx, multiboot_info_ptr

/*Load flags into eax*/
movl (%ebx), %eax

/*Test bit 2 for a cmdline pointer*/
bt $2, %eax
jnc no_cmdline
movl 16(%ebx), %ecx
add $0xc0000000, %ecx
movl %ecx, 16(%ebx)

no_cmdline:
/*Test bit 3 for a module pointer*/
bt $3, %eax
jnc no_mods
movl 24(%ebx), %ecx
add $0xc0000000, %ecx
movl %ecx, 24(%ebx)

no_mods:
/*Test bit 4 for an a.out symbol table*/
bt $4, %eax
jnc no_aout
movl 36(%ebx), %ecx
add $0xc0000000, %ecx
movl %ecx, 36(%ebx)

no_aout:
/*Test bit 5 for an elf symbol table*/
bt $5, %eax
jnc no_elf
movl 36(%ebx), %ecx
add $0xc0000000, %ecx
movl %ecx, 36(%ebx)

no_elf:
/*Test bit 6 for a mmap table*/
bt $6, %eax
jnc no_mmap
movl 48(%ebx), %ecx
add $0xc0000000, %ecx
movl %ecx, 48(%ebx)

no_mmap:
/*Test bit 7 for a drive table*/
bt $7, %eax
jnc no_drive
movl 56(%ebx), %ecx
add $0xc0000000, %ecx
movl %ecx, 56(%ebx)

no_drive:
/*Test bit 9 for a bootlader name*/
bt $9, %eax
jnc no_bootname
movl 64(%ebx), %ecx
add $0xc0000000, %ecx
movl %ecx, 64(%ebx)

no_bootname:
/*Test bit 10 for an apm table*/
bt $10, %eax
jnc no_apm
movl 68(%ebx), %ecx
add $0xc0000000, %ecx
movl %ecx, 68(%ebx)

no_apm:

call _init

call kernel_main /*jump to cpp*/

cli
halt: hlt
jmp halt

.size _start, . - _start
