/**
 * @file io.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-13
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <assert.h>
#include <kernel/io.h>

namespace Kernel {
namespace IO {

/**
 * @brief Reads 8 bits from a port
 *
 * @param port Port to read from
 * @return unsigned char The value read from the port
 */
__attribute__((no_caller_saved_registers)) unsigned char in8(int port) {
    char value;
    asm volatile("inb %1, %0" : "=a"(value) : "Nd"(port));
    return value;
}

/**
 * @brief Reads 16 bits from a port
 *
 * @param port Port to read from
 * @return unsigned short The value read from the port
 */
__attribute__((no_caller_saved_registers)) unsigned short
in16(int port) {
    short value;
    asm volatile("inw %1, %0" : "=ax"(value) : "Nd"(port));
    return value;
}

/**
 * @brief Writes 8 bits to a port
 *
 * @param port Port to write to
 * @param value The value to write to the port
 */
__attribute__((no_caller_saved_registers)) void
out8(int port, unsigned char value) {
    asm volatile("outb %0, %1" ::"a"(value), "Nd"(port));
}

/**
 * @brief Writes 16 bits to a port
 *
 * @param port Port to write to
 * @param value The value to write to the port
 */
__attribute__((no_caller_saved_registers)) void
out16(int port, unsigned short value) {
    asm volatile("outw %0, %1" ::"a"(value), "Nd"(port));
}

} // namespace IO
} // namespace Kernel