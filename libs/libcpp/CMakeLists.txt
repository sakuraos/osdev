set(LIBKPP_SOURCES 
	libcpp_init.cpp
	new/new.cpp
	)

set(LIBCPP_SOURCES
	${LIBKPP_SOURCES}
)

add_library(libkpp STATIC ${LIBKPP_SOURCES})
target_compile_definitions(libkpp PRIVATE __is_libkpp)
target_include_directories(libkpp PUBLIC include)
target_link_libraries(libkpp libk)

add_library(libcpp STATIC ${LIBCPP_SOURCES})
target_compile_definitions(libcpp PRIVATE __is_libcpp)
target_include_directories(libcpp PUBLIC include)
target_link_libraries(libcpp libc)
