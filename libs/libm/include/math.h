/**
 * @file math.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-12-08
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef MATH_H
#define MATH_H

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

#define FP_NAN       0
#define FP_INFINITE  1
#define FP_ZERO      2
#define FP_SUBNORMAL 3
#define FP_NORMAL    4

double      fabs(double);
float       fabsf(float);
long double fabsl(long double);
double      fmod(double, double);
float       fmodf(float, float);
long double fmodl(long double, long double);
double      remainder(double, double);
float       remainderf(float, float);
long double remainderl(long double, long double);
double      remquo(double, double);
float       remquof(float, float);
long double remquol(long double, long double);
double      fma(double, double, double);
float       fmaf(float, float);
long double fmal(long double, long double);
double      fmax(double, double);
float       fmaxf(float, float);
long double fmaxl(long double, long double);
double      fmin(double, double);
float       fminf(float, float);
long double fminl(long double, long double);
double      fdim(double, double);
float       fdimf(float, float);
long double fdiml(long double, long double);
double      nan(const char *);
float       nanf(const char *);
long double nanl(const char *);
double      exp(double);
float       expf(float);
long double expl(long double);
double      exp2(double);
float       exp2f(float);
long double exp2l(long double);
double      expm1(double);
float       expm1f(float);
long double expm1l(long double);
double      log(double);
float       logf(float);
long double logl(long double);
double      log10(double);
float       log10f(float);
long double log10l(long double);
double      log2(double);
float       log2f(float);
long double log2l(long double);
double      log1p(double);
float       log1pf(float);
long double log1pl(long double);
double      pow(double, double);
float       powf(float, float);
long double powl(long double, long double);
double      sqrt(double);
float       sqrtf(float);
long double sqrtl(long double);
double      cbrt(double);
float       cbrtf(float);
long double cbrtl(long double);
double      hypot(double, double);
float       hypotf(float, float);
long double hypotl(long double, long double);
double      sin(double);
float       sinf(float);
long double sinl(long double);
double      cos(double);
float       cosf(float);
long double cosl(long double);
double      tan(double);
float       tanf(float);
long double tanl(long double);
double      asin(double);
float       asinf(float);
long double asinl(long double);
double      acos(double);
float       acosf(float);
long double acosl(long double);
double      atan(double);
float       atanf(float);
long double atanl(long double);
double      atan2(double, double);
float       atan2f(float, float);
long double atan2l(long double, long double);
double      sinh(double);
float       sinhf(float);
long double sinhl(long double);
double      cosh(double);
float       coshf(float);
long double coshl(long double);
double      tanh(double);
float       tanhf(float);
long double tanhl(long double);
double      asinh(double);
float       asinhf(float);
long double asinhl(long double);
double      acosh(double);
float       acoshf(float);
long double acoshl(long double);
double      atanh(double);
float       atanhf(float);
long double atanhl(long double);
double      erf(double);
float       erff(float);
long double erfl(long double);
double      erfc(double);
float       erfcf(float);
long double erfcl(long double);
double      tgamma(double);
float       tgammaf(float);
long double tgammal(long double);
double      lgamma(double);
float       lgammaf(float);
long double lgammal(long double);
double      ceil(double);
float       ceilf(float);
long double ceill(long double);
double      floor(double);
float       floorf(float);
long double floorl(long double);
double      trunc(double);
float       truncf(float);
long double truncl(long double);
double      round(double);
float       roundf(float);
long double roundl(long double);
long        lround(double);
long        lroundf(float);
long        lroundl(long double);
long long   llround(double);
long long   llroundf(float);
long long   llroundl(long double);
double      nearbyint(double);
float       nearbyintf(float);
long double nearbyintl(long double);
double      rint(double);
float       rintf(float);
long double rintl(long double);
long        lrint(double);
long        lrintf(float);
long        lrintl(long double);
long long   llrint(double);
long long   llrintf(float);
long long   llrintl(long double);
double      frexp(double, int *);
float       frexpf(float, int *);
long double frexpl(long double, int *);
double      ldexp(double, int);
float       ldexpf(float, int);
long double ldexpl(long double, int);
double      modf(double, double *);
float       modff(float, float *);
long double modfl(long double, long double *);
double      scalbn(double);
float       scalbnf(float);
long double scalbnl(long double);
double      scalbln(double);
float       scalblnf(float);
long double scalblnl(long double);
int         ilogb(double);
int         ilogbf(float);
int         ilogbl(long double);
double      logb(double);
float       logbf(float);
long double logbl(long double);
double      nextafter(double);
float       nextafterf(float);
long double nextafterl(long double);
double      nexttoward(double);
float       nexttowardf(float);
long double nexttowardl(long double);
double      copysign(double);
float       copysignf(float);
long double copysignl(long double);
// fpclassify
// isfinite
// isinf
// isnan
// isnormal
// signbit
// isgreater
// isgreaterequal
// isless
// islessequal
// islessgreater
// isunordered

#ifdef __cplusplus
}
#endif

#endif