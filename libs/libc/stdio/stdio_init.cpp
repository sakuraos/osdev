/**
 * @file stdio_init.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-17
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <assert.h>
#include <stdio.h>

#ifdef __is_libk
#include <kernel/serial.h>
#include <kernel/vga.h>
int read(void *, char *, int) { ASSERT_NOT_REACHED; }
int write(void *, const char *str, int) {
    Kernel::VGA::terminal_putchar(str[0]);
    if (Kernel::Serial::get_debug())
        Kernel::Serial::putchar(str[0]);
    return 0;
}
int write_err(void *, const char *str, int) {
    Kernel::VGA::terminal_putchar(str[0]);
    Kernel::Serial::putchar(str[0]);
    return 0;
}
#else
int read(void *, char *, int) { ASSERT_NOT_REACHED; }
int write(void *, const char *, int) { ASSERT_NOT_REACHED; }
int write_err(void *, const char *, int) { ASSERT_NOT_REACHED; }
#endif

FILE __sF[3] = {
    {0, 0, 0,      __SRD, 0, {0, 0},    0,   __sF + 0, 0, read,
     0, 0, {0, 0}, 0,     0, {0, 0, 0}, {0}, {0, 0},   0, 0}, // stdin
    {0,      0, 0, __SWR,     0,   {0, 0}, 0, __sF + 0, 0, 0, 0, write,
     {0, 0}, 0, 0, {0, 0, 0}, {0}, {0, 0}, 0, 0}, // stdout
    {0,      0,      0, __SWR | __SNBF,
     0,      {0, 0}, 0, __sF + 0,
     0,      0,      0, write_err,
     {0, 0}, 0,      0, {0, 0, 0},
     {0},    {0, 0}, 0, 0} // stderr
};
