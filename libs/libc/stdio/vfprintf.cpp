/**
 * @file vfprintf.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdio.h>
#include <stdlib.h>

extern "C" int vfprintf(FILE *__restrict fp,
                        const char *__restrict format, va_list ap) {

    for (const char *it = format; *it != 0; it++) {
        if (*it != '%')
            fputc(*it, fp);
        else {
            //*it == %
            it++;
            char str[33];
            switch (*it) {
            case '%':
                fputc('%', fp);
                break;
            // d, i
            case 'd':
            case 'i':
                itoa(va_arg(ap, int), str, 10);
                for (const char *i = str; *i != 0; i++)
                    fputc(*i, fp);
                break;
                // u
            case 'u':
                itoa(va_arg(ap, int), str, 10);
                for (const char *i = str; *i != 0; i++)
                    fputc(*i, fp);
                break;
                // o
            case 'o':
                char str[33];
                itoa(va_arg(ap, int), str, 8);
                for (const char *i = str; *i != 0; i++)
                    fputc(*i, fp);
                break;
                // x, p
            case 'x':
            case 'p':
                itoa(va_arg(ap, int), str, 16);
                for (const char *i = str; *i != 0; i++)
                    fputc(*i, fp);
                break;
                // X
            case 'X':
                break;
                // f
            case 'f':
                break;
                // F
            case 'F':
                break;
                // e
            case 'e':
                break;
                // E
            case 'E':
                break;
                // g
            case 'g':
                break;
                // G
            case 'G':
                break;
                // a
            case 'a':
                break;
                // A
            case 'A':
                break;
                // c
            case 'c':
                fputc(va_arg(ap, int), fp);
                break;
                // s
            case 's':
                for (const char *i = va_arg(ap, char *); *i != 0; i++)
                    fputc(*i, fp);
                break;
                // n
            case 'n':
                break;
            }
        }
    }
    return EOF;
}
