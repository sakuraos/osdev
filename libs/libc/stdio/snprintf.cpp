/**
 * @file snprintf.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdio.h>

extern "C" int snprintf(char *__restrict str, size_t size,
                        const char *__restrict format, ...) {
    int     len;
    va_list args;
    va_start(args, format);
    len = vsnprintf(str, size, format, args);
    va_end(args);
    return len;
}
