/**
 * @file puts.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */
#include <stdio.h>

extern "C" int puts(const char *str) {
    for (const char *it = str; *it != 0; it++) {
        putchar(*it);
    }
    putchar('\n');
    return 0;
}
