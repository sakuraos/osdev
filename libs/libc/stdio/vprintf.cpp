/**
 * @file vprintf.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdio.h>

extern "C" int vprintf(const char *__restrict format, va_list ap) {
    return vfprintf(stdout, format, ap);
}
