/**
 * @file putchar.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-13
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdio.h>

extern "C" int putchar(int ch) { return fputc(ch, stdout); }