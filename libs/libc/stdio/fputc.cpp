/**
 * @file fputc.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-13
 *
 * @copyright Copyright (c) 2021
 *
 */
#include <assert.h>
#include <stdio.h>

extern "C" int fputc(int ch, FILE *fp) {
    char tmp[2] = {(char)ch, 0};
    return fp->_write(fp->_cookie, tmp, fp->_file);
}
