/**
 * @file perror.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-10
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>

extern "C" void perror(const char *str) {
    fprintf(stderr, "%s : %s\n", str, strerror(errno));
}