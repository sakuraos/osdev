/**
 * @file fdopen.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-30
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <stdio.h>

extern "C" FILE *fdopen(int fd, const char *mode) {}