/**
 * @file fread.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-30
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <stdio.h>

extern "C" size_t fread(void *, size_t, size_t, FILE *) {}