/**
 * @file printf.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdio.h>

extern "C" int printf(const char *__restrict format, ...) {
    int len = 0;

    va_list args;
    va_start(args, format);
    len = vprintf(format, args);
    va_end(args);
    return len;
}
