/**
 * @file assertion.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-13
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <assert.h>
#include <stdio.h>

[[noreturn]] void _assertion_failed(const char *msg) {
    fprintf(stderr, "%s", msg);
#ifdef __is_libk
    asm volatile(R"(cli
                 1: hlt
                 jmp 1b)");
#else
    asm volatile("ud2");
#endif
    __builtin_unreachable();
}