/**
 * @file stdlib.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-07
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef STDLIB_H
#define STDLIB_H

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _s_div {
    int quot;
    int rem;
} div_t;
typedef struct _s_ldiv {
    long quot;
    long rem;
} ldiv_t;
typedef struct _s_lldiv {
    long long quot;
    long long rem;
} lldiv_t;

char *itoa(unsigned int, char *, int);

int  rand();
void srand(unsigned int);

void *calloc(size_t, size_t);
void *realloc(void *, size_t);
void *malloc(size_t);
void  free(void *);
void  abort();
void  exit(int);
char *getenv(const char *);

int           atexit(void (*func)(void));
double        atof(const char *str);
int           atoi(const char *str);
long          atol(const char *str);
void         *bsearch(const void *key, const void *ptr, size_t count,
                      size_t size, int (*comp)(const void *, const void *));
void          qsort(void *ptr, size_t count, size_t size,
                    int (*comp)(const void *, const void *));
double        strtod(const char *str, char **str_end);
float         strtof(const char *str, char **str_end);
long          strtol(const char *str, char **str_end, int base);
unsigned long strtoul(const char *str, char **str_end, int base);
int           system(const char *command);

int       abs(int);
long      labs(long);
long long llabs(long long);
div_t     div(int, int);
ldiv_t    ldiv(long, long);
lldiv_t   lldiv(long long, long long);

#ifdef __cplusplus
}
#endif
#endif
