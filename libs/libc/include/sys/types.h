/**
 * @file types.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-09-29
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef SYS_TYPES_H
#define SYS_TYPES_H

typedef int gid_t;
typedef int off_t;
typedef int pid_t;

typedef int mode_t;
typedef int dev_t;
typedef int ino_t;
typedef int nlink_t;
typedef int blksize_t;
typedef int blkcnt_t;

typedef unsigned int size_t;
typedef int          ssize_t;

typedef int uid_t;

typedef int intptr_t;

typedef unsigned int clock_t;
typedef long long    time_t;
typedef int          suseconds_t;

#endif