/**
 * @file time.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-02-19
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <sys/types.h>

#ifndef SYS_TIME_H
#define SYS_TIME_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct s_timeval {
    time_t      tv_sec;
    suseconds_t tv_usec;
} timeval;

typedef struct s_itimerval {
    timeval it_interval;
    timeval it_value;
} itimerval;

int getitimer(int, itimerval *);
int gettimeofday(timeval *, void *);
int setitimer(int, const itimerval *, itimerval *);
int utimes(const char *, const timeval[2]);

#ifdef __cplusplus
}
#endif

#endif