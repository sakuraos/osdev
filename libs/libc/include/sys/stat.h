/**
 * @file stat.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-21
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <sys/types.h>
#include <time.h>

#ifndef SYS_STAT_H
#define SYS_STAT_H

#ifdef __cplusplus
extern "C" {
#endif

#define S_IRWXU 0700 /*Read, write, execute/search by owner.*/
#define S_IRUSR 0400 /*Read permission, owner.              */
#define S_IWUSR 0200 /*Write permission, owner.             */
#define S_IXUSR 0100 /*Execute/search permission, owner.    */

#define S_IRWXG 070 /*Read, write, execute/search by group.*/
#define S_IRGRP 040 /*Read permission, group.              */
#define S_IWGRP 020 /*Write permission, group.             */
#define S_IXGRP 010 /*Execute/search permission, group.    */

#define S_IRWXO 07 /*Read, write, execute/search by others.*/
#define S_IROTH 04 /*Read permission, others.             */
#define S_IWOTH 02 /*Write permission, others.            */
#define S_IXOTH 01 /*Execute/search permission, others.   */

#define S_ISUID 04000 /*Set-user-ID on execution.            */
#define S_ISGID 02000 /*Set-group-ID on execution.           */
#define S_ISVTX 01000 /*On directories, restricted deletion flag.*/

struct stat {
    dev_t   st_dev;   /*Device ID of device containing file.*/
    ino_t   st_ino;   /*File serial number.*/
    mode_t  st_mode;  /*Mode of file (see below).*/
    nlink_t st_nlink; /*Number of hard links to the file.*/
    uid_t   st_uid;   /*User ID of file.*/
    gid_t   st_gid;   /*Group ID of file.*/
    dev_t
        st_rdev; /*Device ID (if file is character or block special).*/
    off_t st_size; /*For regular files, the file size in bytes.
 For symbolic links, the length in bytes of the
 pathname contained in the symbolic link.
 For a shared memory object, the length in bytes.
 For a typed memory object, the length in bytes.
 For other file types, the use of this field is
unspecified.*/
    struct timespec st_atim; /*Last data access timestamp.*/
    struct timespec st_mtim; /*Last data modification timestamp.*/
    struct timespec st_ctim; /*Last file status change timestamp.*/
    blksize_t
        st_blksize; /*A file system-specific preferred I/O block size*/
                    /*for this object. In some file system types, this*/
                    /*may vary from file to file.*/
    blkcnt_t st_blocks; /*Number of blocks
                          allocated for
                          this object.*/
};

int chmod(const char *path, mode_t mode);
int mkdir(const char *path, mode_t mode);
int stat(const char *path, struct stat *buf);

#ifdef __cplusplus
}
#endif

#endif
