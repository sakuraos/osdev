/**
 * @file setjmp.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-21
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef SETJMP_H
#define SETJMP_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    int foo;
} jmp_buf;

void longjmp(jmp_buf env, int status);

// setjmp

#ifdef __cplusplus
}
#endif

#endif