/**
 * @file stdio.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */
#include <stdarg.h>
#include <sys/types.h>

#ifndef STDIO_H
#define STDIO_H

#ifdef __cplusplus
extern "C" {
#endif

typedef long long fpos_t;

struct __sbuf {
    unsigned char *_base;
    int            _size;
};

typedef struct __sFILE {
    unsigned char *_p;       // current position in buffer
    int            _r;       // read space left for getc
    int            _w;       // write space left for putc
    short          _flags;   // flags, below
    short          _file;    // unix file number
    struct __sbuf  __bf;     // the buffer
    int            _lbfsize; // 0 or -_bf._size, for inline putc
    void          *_cookie;  // passsed to io functions
    int (*_close)(void *);
    int (*_read)(void *, char *, int);
    fpos_t (*_seek)(void *, fpos_t, int);
    int (*_write)(void *, const char *, int);
    // extension data, to avoid further ABI breakage
    struct __sbuf _ext;
    // data for long sequences of ungetc()
    unsigned char *_up; // saved _p when _p is doing ungetc data
    int            _ur; // saved _r when _r is counting ungetc data
    // tricks to meet minimum requirements even when malloc() fails
    unsigned char _ubuf[3]; // guarantee an ungetc() buffer
    unsigned char _nbuf[1]; // guarantee a getc() buffer
    // separate buffer for fgetln() when line crosses buffer boundary
    struct __sbuf _lb; // buffer for fgetln()
    // Unix stdio files get aligned to block boundaries on fseek()
    int    _blksize; // stat.st_blksize (may be != _bf._size)
    fpos_t _offset;  // current lseek offset
} FILE;

#define __SLBF 0x0001 // line buffered
#define __SNBF 0x0002 // unbuffered
#define __SRD  0x0004 // OK to read
#define __SWR                                                          \
    0x0008            // OK to write
                      // RD and WR are never simultaneously asserted
#define __SRW  0x0010 // open for reading & writing
#define __SEOF 0x0020 // found EOF
#define __SERR 0x0040 // found error
#define __SMBF 0x0080 // _buf is from malloc
#define __SAPP 0x0100 // fdopen()ed in append mode
#define __SSTR 0x0200 // this is an sprintf/snprintf string
#define __SOPT 0x0400 // do fseek() optimisation
#define __SNPT 0x0800 // do not do fseek() optimisation
#define __SOFF 0x1000 // set iff _offset is in fact correct
#define __SMOD 0x2000 // true => fgetln modified _p text
#define __SALC 0x4000 // allocate string space dynamically
#define __SIGN 0x8000 // ignore this file in _fwalk

#ifndef EOF
#define EOF (-1)
#endif

#define SEEK_SET 1
#define SEEK_CUR 2
#define SEEK_END 4

#define BUFSIZ 0

#define _IOFBF 1
#define _IOLBF 2
#define _IONBF 4

extern FILE __sF[];

#define stdin  (&__sF[0])
#define stdout (&__sF[1])
#define stderr (&__sF[2])

int  putchar(int);
int  puts(const char *);
int  printf(const char *__restrict, ...);
int  fprintf(FILE *__restrict, const char *__restrict, ...);
int  sprintf(char *__restrict, const char *__restrict, ...);
int  snprintf(char *__restrict, size_t, const char *__restrict, ...);
int  vprintf(const char *__restrict, va_list ap);
int  vfprintf(FILE *__restrict, const char *__restrict, va_list ap);
int  vsprintf(char *__restrict, const char *__restrict, va_list ap);
int  vsnprintf(char *__restrict, size_t, const char *__restrict,
               va_list ap);
void perror(const char *);
int  fputc(int, FILE *);

FILE  *fopen(const char *, const char *);
int    fclose(FILE *);
size_t fread(void *, size_t, size_t, FILE *);
size_t fwrite(const void *, size_t, size_t, FILE *);
int    fseek(FILE *, long, int);
long   ftell(FILE *);
void   setbuf(FILE *, char *);

void  clearerr(FILE *stream);
int   feof(FILE *stream);
int   ferror(FILE *stream);
int   fflush(FILE *stream);
int   fgetc(FILE *stream);
int   fgetpos(FILE *stream, fpos_t *pos);
char *fgets(char *str, int count, FILE *stream);
int   fputs(const char *str, FILE *stream);
FILE *freopen(const char *filename, const char *mode, FILE *stream);
int   fscanf(FILE *stream, const char *format, ...);
int   fsetpos(FILE *stream, const fpos_t *pos);
int   getc(FILE *stream);
int   getchar(void);
int   putc(int ch, FILE *stream);
int   remove(const char *fname);
int   rename(const char *old_filename, const char *new_filename);
void  rewind(FILE *stream);
int   scanf(const char *format, ...);
int   setvbuf(FILE *stream, char *buffer, int mode, size_t size);
int   sscanf(const char *buffer, const char *format, ...);
FILE *tmpfile(void);
int   ungetc(int ch, FILE *stream);

FILE *fdopen(int fd, const char *mode);
int   fileno(FILE *stream);

#ifdef __cplusplus
}
#endif

#endif
