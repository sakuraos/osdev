/**
 * @file assert.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-12
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef ASSERT_H
#define ASSERT_H

#define __STRINGIFY_HELPER(x) #x
#define __STRINGIFY(x)        __STRINGIFY_HELPER(x)

[[noreturn]] void _assertion_failed(const char *);

#define ASSERT(expr)                                                   \
    (expr ? void(0)                                                    \
          : _assertion_failed(__STRINGIFY(                             \
                expr) ":" __FILE__ ":" __STRINGIFY(__LINE__) "\n"))
#define ASSERT_NOT_REACHED ASSERT(false)
#define TODO               ASSERT_NOT_REACHED

#define assert(expr) ASSERT(expr)

#endif