/**
 * @file inttypes.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-12-07
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdint.h>

#ifndef INTTYPES_H
#define INTTYPES_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _s_imaxdiv {
    intmax_t quot;
    intmax_t rem;
} imaxdiv_t;

intmax_t  imaxabs(intmax_t);
imaxdiv_t imaxdiv(intmax_t, intmax_t);

#ifdef __cplusplus
}
#endif

#endif