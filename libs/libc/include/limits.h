/**
 * @file limits.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-12-08
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef LIMITS_H
#define LIMITS_H

#define INT_MAX 2147483647
#define INT_MIN -2147483648

#endif