/**
 * @file errno.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-10
 *
 * @copyright Copyright (c) 2021
 *
 */

#ifndef ERRNO_H
#define ERRNO_H

extern int errno;

#define errno errno

extern const char *const sys_errlist[];

#define __RETURN_WITH_ERRNO(rc, good_ret, bad_ret)                     \
    do {                                                               \
        if (rc < 0) {                                                  \
            errno = -rc;                                               \
            return (bad_ret);                                          \
        }                                                              \
        errno = 0;                                                     \
        return (good_ret);                                             \
    } while (0)

enum ErrnoCode {
    ESUCCESS,
#define ESUCCESS ESUCCESS
    EDOM,
#define EDOM EDOM
    ERANGE,
#define ERANGE ERANGE
    EILSEQ,
#define EILSEQ EILSEQ
    ENOSYS,
#define ENOSYS ENOSYS
    EAFNOSUPPORT,
#define EAFNOSUPPORT EAFNOSUPPORT
    EADDRINUSE,
#define EADDRINUSE EADDRINUSE
    EADDRNOTAVAIL,
#define EADDRNOTAVAIL EADDRNOTAVAIL
    EISCONN,
#define EISCONN EISCONN
    E2BIG,
#define E2BIG E2BIG
    EFAULT,
#define EFAULT EFAULT
    EBADF,
#define EBADF EBADF
    EPIPE,
#define EPIPE EPIPE
    ECONNABORTED,
#define ECONNABORTED ECONNABORTED
    EALREADY,
#define EALREADY EALREADY
    ECONNREFUSED,
#define ECONNREFUSED ECONNREFUSED
    ECONNRESET,
#define ECONNRESET ECONNRESET
    EXDEV,
#define EXDEV EXDEV
    EDESTADDRREQ,
#define EDESTADDRREQ EDESTADDRREQ
    EBUSY,
#define EBUSY EBUSY
    ENOTEMPTY,
#define ENOTEMPTY ENOTEMPTY
    ENOEXEC,
#define ENOEXEC ENOEXEC
    EEXIST,
#define EEXIST EEXIST
    EFBIG,
#define EFBIG EFBIG
    ENAMETOOLONG,
#define ENAMETOOLONG ENAMETOOLONG
    EHOSTUNREACH,
#define EHOSTUNREACH EHOSTUNREACH
    ENOTTY,
#define ENOTTY ENOTTY
    EINTR,
#define EINTR EINTR
    EINVAL,
#define EINVAL EINVAL
    ESPIPE,
#define ESPIPE ESPIPE
    EIO,
#define EIO EIO
    EISDIR,
#define EISDIR EISDIR
    EMSGSIZE,
#define EMSGSIZE EMSGSIZE
    ENETDOWN,
#define ENETDOWN ENETDOWN
    ENETRESET,
#define ENETRESET ENETRESET
    ENETUNREACH,
#define ENETUNREACH ENETUNREACH
    ENOBUFS,
#define ENOBUFS ENOBUFS
    ECHILD,
#define ECHILD ECHILD
    ENOLCK,
#define ENOLCK ENOLCK
    ENOMSG,
#define ENOMSG ENOMSG
    ENOPROTOOPT,
#define ENOPROTOOPT ENOPROTOOPT
    ENOSPC,
#define ENOSPC ENOSPC
    ENXIO,
#define ENXIO ENXIO
    ENODEV,
#define ENODEV ENODEV
    ENOENT,
#define ENOENT ENOENT
    ESRCH,
#define ESRCH ESRCH
    ENOTDIR,
#define ENOTDIR ENOTDIR
    ENOTSOCK,
#define ENOTSOCK ENOTSOCK
    ENOTCONN,
#define ENOTCONN ENOTCONN
    ENOMEM,
#define ENOMEM ENOMEM
    EINPROGRESS,
#define EINPROGRESS EINPROGRESS
    EPERM,
#define EPERM EPERM
    EOPNOTSUPP,
#define EOPNOTSUPP EOPNOTSUPP
    EWOULDBLOCK,
#define EWOULDBLOCK EWOULDBLOCK
    EACCES,
#define EACCES EACCES
    EPROTONOSUPPORT,
#define EPROTONOSUPPORT EPROTONOSUPPORT
    EROFS,
#define EROFS EROFS
    EDEADLK,
#define EDEADLK EDEADLK
    EAGAIN,
#define EAGAIN EAGAIN
    ETIMEDOUT,
#define ETIMEDOUT ETIMEDOUT
    ENFILE,
#define ENFILE ENFILE
    EMFILE,
#define EMFILE EMFILE
    EMLINK,
#define EMLINK EMLINK
    ELOOP,
#define ELOOP ELOOP
    EPROTOTYPE,
#define EPROTOTYPE EPROTOTYPE
    EOVERFLOW,
#define EOVERFLOW EOVERFLOW
    ENOTSUP,
#define ENOTSUP ENOTSUP
    EMAXERRNO
#define EMAXERRNO EMAXERRNO
};

#endif
