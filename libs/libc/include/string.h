/**
 * @file string.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <sys/types.h>

#ifndef STRING_H
#define STRING_H

#ifdef __cplusplus
extern "C" {
#endif

size_t strlen(const char *);
char  *strcpy(char *, const char *);
char  *strncpy(char *, const char *, size_t);
char  *strerror(int);
void  *memmove(void *, const void *, size_t);
void  *memcpy(void *, const void *, size_t);
void  *memset(void *, int, size_t);
int    strcmp(const char *, const char *);
int    strncmp(const char *, const char *, size_t);
char  *strchr(const char *, int);
char  *strrchr(const char *, int);
char  *strcat(char *, const char *);
char  *strncat(char *, const char *, size_t);

void  *memchr(const void *ptr, int ch, size_t count);
int    memcmp(const void *lhs, const void *rhs, size_t count);
int    strcoll(const char *lhs, const char *rhs);
size_t strcspn(const char *dest, const char *src);
size_t strspn(const char *dest, const char *src);
char  *strtok(char *str, const char *delim);
size_t strxfrm(char *dest, const char *src, size_t count);
char  *strpbrk(const char *dest, const char *breakset);
char  *strstr(const char *str, const char *substr);

#ifdef __cplusplus
}
#endif

#endif
