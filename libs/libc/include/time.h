/**
 * @file time.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-21
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef TIME_H
#define TIME_H

#include <stdint.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

struct tm {
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday;
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
    int tm_isdst;
};

struct timespec {
    time_t tv_sec;  /* Seconds */
    long   tv_nsec; /* Nanoseconds */
};

clock_t    clock(void);
double     difftime(time_t time_end, time_t time_beg);
time_t     mktime(struct tm *time);
time_t     time(time_t *arg);
char      *asctime(const struct tm *time_ptr);
char      *ctime(const time_t *timer);
struct tm *gmtime(const time_t *timer);
struct tm *localtime(const time_t *timer);
size_t     strftime(char *str, size_t count, const char *format,
                    const struct tm *time);

#ifdef __cplusplus
}
#endif

#endif