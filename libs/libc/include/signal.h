/**
 * @file signal.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-21
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef SIGNAL_H
#define SIGNAL_H

#ifdef __cplusplus
extern "C" {
#endif

typedef int sig_atomic_t;

void (*signal(int sig, void (*handler)(int)))(int);
int raise(int sig);

#ifdef __cplusplus
}
#endif

#endif