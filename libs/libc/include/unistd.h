/**
 * @file unistd.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-09-29
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <sys/types.h>

#ifndef UNISTD_H
#define UNISTD_H

#ifdef __cplusplus
extern "C" {
#endif

ssize_t write(int, const void *, size_t);

ssize_t pread(int fildes, void *buf, size_t nbyte, off_t offset);
ssize_t read(int fildes, void *buf, size_t nbyte);

off_t lseek(int fildes, off_t offset, int whence);

int close(int fd);

pid_t getpid(void);

char *getcwd(char *buf, size_t size);
int   chdir(const char *path);

int isatty(int fd);

#ifdef __cplusplus
}
#endif

#endif
