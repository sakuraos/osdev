/**
 * @file iconv.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-21
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <sys/types.h>

#ifndef ICONV_H
#define ICONV_H

#ifdef __cplusplus
extern "C" {
#endif

typedef void *iconv_t;

extern iconv_t iconv_open(const char *tocode, const char *fromcode);
extern size_t  iconv(iconv_t, char **inbuf, size_t *inbytesleft,
                     char **outbuf, size_t *outbytesleft);
extern int     iconv_close(iconv_t);

#ifdef __cplusplus
}
#endif

#endif