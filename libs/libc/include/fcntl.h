/**
 * @file fcntl.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-21
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef FCNTL_H
#define FCNTL_H

#ifdef __cplusplus
extern "C" {
#endif

#define F_DUPFD  0
#define F_GETFD  1
#define F_SETFD  2
#define F_GETFL  3
#define F_SETFL  4
#define F_ISTTY  5
#define F_GETLK  6
#define F_SETLK  7
#define F_SETLKW 8

#define FD_CLOEXEC 1

#define O_RDONLY    (1 << 0)
#define O_WRONLY    (1 << 1)
#define O_RDWR      (O_RDONLY | O_WRONLY)
#define O_ACCMODE   (O_RDONLY | O_WRONLY)
#define O_EXEC      (1 << 2)
#define O_CREAT     (1 << 3)
#define O_EXCL      (1 << 4)
#define O_NOCTTY    (1 << 5)
#define O_TRUNC     (1 << 6)
#define O_APPEND    (1 << 7)
#define O_NONBLOCK  (1 << 8)
#define O_DIRECTORY (1 << 9)
#define O_NOFOLLOW  (1 << 10)
#define O_CLOEXEC   (1 << 11)
#define O_DIRECT    (1 << 12)

int open(const char *path, int options, ...);

#ifdef __cplusplus
}
#endif

#endif