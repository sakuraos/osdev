/**
 * @file ctype_init.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-21
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <ctype.h>

const char _ctype_[256] = {_C,      _C,      _C,
                           _C,      _C,      _C,
                           _C,      _C,      _C,
                           _C | _S, _C | _S, _C | _S,
                           _C | _S, _C | _S, _C,
                           _C,      _C,      _C,
                           _C,      _C,      _C,
                           _C,      _C,      _C,
                           _C,      _C,      _C,
                           _C,      _C,      _C,
                           _C,      _C,      (char)(_S | _B),
                           _P,      _P,      _P,
                           _P,      _P,      _P,
                           _P,      _P,      _P,
                           _P,      _P,      _P,
                           _P,      _P,      _P,
                           _N,      _N,      _N,
                           _N,      _N,      _N,
                           _N,      _N,      _N,
                           _N,      _P,      _P,
                           _P,      _P,      _P,
                           _P,      _P,      _U | _X,
                           _U | _X, _U | _X, _U | _X,
                           _U | _X, _U | _X, _U,
                           _U,      _U,      _U,
                           _U,      _U,      _U,
                           _U,      _U,      _U,
                           _U,      _U,      _U,
                           _U,      _U,      _U,
                           _U,      _U,      _U,
                           _U,      _P,      _P,
                           _P,      _P,      _P,
                           _P,      _L | _X, _L | _X,
                           _L | _X, _L | _X, _L | _X,
                           _L | _X, _L,      _L,
                           _L,      _L,      _L,
                           _L,      _L,      _L,
                           _L,      _L,      _L,
                           _L,      _L,      _L,
                           _L,      _L,      _L,
                           _L,      _L,      _L,
                           _P,      _P,      _P,
                           _P,      _C};