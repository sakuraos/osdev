/**
 * @file gettimeofday.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-02-19
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <sys/time.h>

extern "C" int gettimeofday(timeval *, void *) {}