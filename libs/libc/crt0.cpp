/**
 * @file crt0.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-29
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <stdlib.h>

extern "C" {
extern void _init(void);
int         main(int, char **, char **);

// Tell the compiler that this may be called from somewhere else.
int  _entry(int argc, char **argv, char **env) __attribute__((used));
void _start(int, char **, char **) __attribute__((used));

void _start(int, char **, char **) {
    asm("push $0\n"
        "jmp _entry@plt\n");
}

int _entry(int argc, char **argv, char **env) {
    _init();
    exit(main(argc, argv, env));
}
}