/**
 * @file setlocale.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-30
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <locale.h>

extern "C" char *setlocale(int, const char *) {}