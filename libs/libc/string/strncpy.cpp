/**
 * @file strncpy.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-10
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <string.h>

extern "C" char *strncpy(char *dest, const char *src, size_t n) {
    char end = 0;
    for (size_t idx = 0; idx < n; idx++) {
        if (src[idx] == 0) {
            end = 1;
        }
        if (end == 1) {
            dest[idx] = 0;
        } else {
            dest[idx] = src[idx];
        }
    }
    return dest;
}