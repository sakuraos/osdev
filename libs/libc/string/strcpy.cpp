/**
 * @file strcpy.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-10
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <string.h>

extern "C" char *strcpy(char *dest, const char *src) {
    size_t idx = 0;
    while (src[idx] != 0) {
        dest[idx] = src[idx];
        idx++;
    }
    dest[idx] = src[idx];
    return dest;
}