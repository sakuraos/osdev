/**
 * @file memmove.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-08-08
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdint.h>
#include <string.h>

extern "C" void *memmove(void *dest, const void *src, size_t num) {
    uintptr_t udest = (uintptr_t)dest;
    uintptr_t usrc  = (uintptr_t)src;
    for (size_t i = 0; i < num; i++) {
        *(char *)udest = *(char *)usrc;
        udest += 1;
        usrc += 1;
    }
    return dest;
}
