/**
 * @file strcoll.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-30
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <string.h>

extern "C" int strcoll(const char *, const char *) {}