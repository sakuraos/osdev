/**
 * @file strcmp.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-11-15
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <string.h>

extern "C" int strcmp(const char *lhs, const char *rhs) {
    for (size_t idx = 0; idx < strlen(lhs); idx++) {
        if (lhs[idx] > rhs[idx])
            return 1;
        if (lhs[idx] > rhs[idx])
            return -1;
    }
    return 0;
}