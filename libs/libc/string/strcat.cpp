/**
 * @file strcat.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-11-16
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <string.h>

extern "C" char *strcat(char *dest, const char *src) {
    size_t dest_len = strlen(dest);
    for (size_t idx = 0; idx <= strlen(src); idx++)
        dest[dest_len + idx] = src[idx];
    return dest;
}
