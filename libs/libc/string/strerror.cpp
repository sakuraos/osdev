/**
 * @file strerror.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-10
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <errno.h>
#include <string.h>

const char *const sys_errlist[] = {
    /*ESUCCESS*/ "Success (not an error)",
    /*EDOM*/ "Math argument out of domain",
    /*ERANGE*/ "Range error",
    /*EILSEQ*/ "Illegal byte sequence",
    /*ENOSYS*/ "Invalid Syscall",
    /*EAFNOSUPPORT*/ "",
    /*EADDRINUSE*/ "",
    /*EADDRNOTAVAIL*/ "",
    /*EISCONN*/ "",
    /*E2BIG*/ "",
    /*EFAULT*/ "",
    /*EBADF*/ "",
    /*EPIPE*/ "",
    /*ECONNABORTED*/ "",
    /*EALREADY*/ "",
    /*ECONNREFUSED*/ "",
    /*ECONNRESET*/ "",
    /*EXDEV*/ "",
    /*EDESTADDRREQ*/ "",
    /*EBUSY*/ "",
    /*ENOTEMPTY*/ "",
    /*ENOEXEC*/ "",
    /*EEXIST*/ "",
    /*EFBIG*/ "",
    /*ENAMETOOLONG*/ "",
    /*EHOSTUNREACH*/ "",
    /*ENOTTY*/ "",
    /*EINTR*/ "",
    /*EINVAL*/ "",
    /*ESPIPE*/ "",
    /*EIO*/ "",
    /*EISDIR*/ "",
    /*EMSGSIZE*/ "",
    /*ENETDOWN*/ "",
    /*ENETRESET*/ "",
    /*ENETUNREACH*/ "",
    /*ENOBUFS*/ "",
    /*ECHILD*/ "",
    /*ENOLCK*/ "",
    /*ENOMSG*/ "",
    /*ENOPROTOOPT*/ "",
    /*ENOSPC*/ "",
    /*ENXIO*/ "",
    /*ENODEV*/ "",
    /*ENOENT*/ "",
    /*ESRCH*/ "",
    /*ENOTDIR*/ "",
    /*ENOTSOCK*/ "",
    /*ENOTCONN*/ "",
    /*ENOMEM*/ "",
    /*EINPROGRESS*/ "",
    /*EPERM*/ "",
    /*EOPNOTSUPP*/ "",
    /*EWOULDBLOCK*/ "",
    /*EACCES*/ "",
    /*EPROTONOSUPPORT*/ "",
    /*EROFS*/ "",
    /*EDEADLK*/ "",
    /*EAGAIN*/ "",
    /*ETIMEDOUT*/ "",
    /*ENFILE*/ "",
    /*EMFILE*/ "",
    /*EMLINK*/ "",
    /*ELOOP*/ "",
    /*EPROTOTYPE*/ "",
    /*EOVERFLOW*/ "",
    /*ENOTSUP*/ "",
    /*EMAXERRNO*/ "Something broke :("};

extern "C" char *strerror(int errnum) {
    if (errnum < 0 || errnum >= EMAXERRNO) {
        return const_cast<char *>("Unknown Error");
    }
    return const_cast<char *>(sys_errlist[errnum]);
}
