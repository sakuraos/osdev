/**
 * @file strchr.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-11-15
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <string.h>

extern "C" char *strchr(const char *str, int ch) {
    for (size_t idx = 0; idx < strlen(str); idx++)
        if (str[idx] == ch)
            return ((char *)str + idx);
    return 0;
}