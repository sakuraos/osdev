/**
 * @file strchr.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-11-15
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <string.h>

extern "C" char *strrchr(const char *str, int ch) {
    for (size_t idx = strlen(str); idx > 0; idx--)
        if (str[idx - 1] == ch)
            return ((char *)str + idx - 1);
    return 0;
}