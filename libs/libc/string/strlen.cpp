/**
 * @file strlen.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-04
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <string.h>

extern "C" size_t strlen(const char *str) {
    size_t len = 0;
    while (str[len])
        len++;
    return len;
}
