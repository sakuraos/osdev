/**
 * @file strncmp.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-11-15
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <string.h>

extern "C" int strncmp(const char *lhs, const char *rhs, size_t count) {
    for (size_t idx = 0; idx < count; idx++) {
        if (lhs[idx] > rhs[idx])
            return 1;
        if (lhs[idx] > rhs[idx])
            return -1;
    }
    return 0;
}