/**
 * @file srand.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-09-07
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdlib.h>

extern int s1;
extern int s2;
extern int s3;

extern "C" void srand(unsigned int seed) {
    s1 = seed;
    s2 = seed;
    s3 = seed;
}