/**
 * @file strtod.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-30
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <stdlib.h>

extern "C" double strtod(const char *str, char **str_end) {}