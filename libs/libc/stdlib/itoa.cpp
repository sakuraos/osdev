/**
 * @file itoa.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-06-07
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <stdlib.h>
#include <string.h>

static char charlut[36] = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
                           '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
                           'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                           'r', 't', 's', 'u', 'v', 'w', 'x', 'y', 'z'};

extern "C" char *itoa(unsigned int value, char *str, int base) {
    char output[33] = {0};
    int  idx        = 0;
    do {
        output[idx] = charlut[value % base];
        value /= base;
        idx++;
    } while (value);
    for (int i = 0; i < (idx + 1) / 2; i++) {
        char temp           = output[i];
        output[i]           = output[idx - 1 - i];
        output[idx - 1 - i] = temp;
    }
    output[idx] = 0;
    strcpy(str, output);
    return str;
}
