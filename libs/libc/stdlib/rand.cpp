/**
 * @file rand.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-09-07
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <limits.h>
#include <stdlib.h>

int s1 = 1;
int s2 = 1;
int s3 = 1;

extern "C" int rand() {
    s1 = (171 * s1) % 30269;
    s2 = (172 * s2) % 30307;
    s3 = (170 * s3) % 30323;
    return (s1 + s2 + s3) % INT_MAX;
}