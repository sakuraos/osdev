/**
 * @file write.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-09-29
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <unistd.h>

extern "C" ssize_t write(int, const void *, size_t) {}