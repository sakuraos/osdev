/**
 * @file lseek.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-30
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <unistd.h>

extern "C" off_t lseek(int fildes, off_t offset, int whence) {}