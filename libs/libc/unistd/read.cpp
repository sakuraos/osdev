/**
 * @file read.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-30
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <unistd.h>

extern "C" ssize_t read(int fildes, void *buf, size_t nbyte) {}