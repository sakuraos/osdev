/**
 * @file cxxabi.cpp
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-30
 *
 * @copyright Copyright (c) 2022
 *
 */

extern "C" {

int __cxa_atexit(void (*exit_function)(void *), void *parameter,
                 void *dso_handle) {}
}