/**
 * @file noncopyable.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2022-01-14
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef NONCOPYABLE_H
#define NONCOPYABLE_H

#define HAB_MAKE_NONCOPYABLE(c)                                        \
  private:                                                             \
    c(const c &)            = delete;                                  \
    c &operator=(const c &) = delete

#define HAB_MAKE_NONMOVABLE(c)                                         \
  private:                                                             \
    c(c &&)            = delete;                                       \
    c &operator=(c &&) = delete

#endif