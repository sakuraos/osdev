/**
 * @file nullor.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-11-27
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <assert.h>

#ifndef NULLOR_H
#define NULLOR_H

namespace HAB {
template <typename T>
class NullOr {
  private:
    T    value;
    bool null;

  public:
    NullOr(T value, bool null) {
        this->value = value;
        this->null  = null;
    }
    bool is_null() { return null; }
    T    get_value() { return value; }
};

} // namespace HAB
#endif // NULLOR_H
