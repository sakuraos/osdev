/**
 * @file ringbuffer.h
 * @author Hanako Athena Beck (@habeck)
 * @brief
 * @version 0.1
 * @date 2021-07-05
 *
 * @copyright Copyright (c) 2021
 *
 */

#include <assert.h>

#ifndef RINGBUFFER_H
#define RINGBUFFER_H

namespace HAB {

template <typename T, unsigned short size = 64>
class RingBuffer {
  private:
    T              m_buffer[size];
    unsigned short m_front;
    unsigned short m_back;
    enum class status {
        buffer_none,
        buffer_full,
        buffer_empty,
    };
    status m_status = status::buffer_empty;

  public:
    T read() {
        ASSERT(m_status != status::buffer_empty);
        T val  = m_buffer[m_back];
        m_back = (m_back + 1) & size;
        if (m_back == m_front)
            m_status = status::buffer_empty;
        else
            m_status = status::buffer_none;
        return val;
    }
    int write(T val) {
        ASSERT(m_status != status::buffer_full);
        m_buffer[m_front] = val;
        m_front           = (m_front + 1) & size;
        if (m_back == m_front) {
            m_status = status::buffer_full;
            return -1;
        } else {
            m_status = status::buffer_none;
            return 0;
        }
    }
    bool is_full() { return m_status == status::buffer_full; }
    bool is_empty() { return m_status == status::buffer_empty; }
};
} // namespace HAB

#endif
