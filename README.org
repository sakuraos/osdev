#+title:README
#+author:Hanako Athena Beck

* SakuraOS

** How to build

*** Build dependencies
- gcc
- make
- bison
- flex
- libgmp
- libmpc
- libmpfr
- texinfo
- cmake
- ninja
- qemu-x86

*** Initial Build steps
1. Clone the repo
  - i.e. `git clone git@gitlab.com:sakuraos/osdev.git`
2. Run `git submodule update --init`
3. Run `./toolchain/build-gcc.sh`
4. Run `./meta/first-build.sh`

*** Testing
1. Build with `ninja -C build` from the project root
2. Run with `ninja -C build run` from the project root
