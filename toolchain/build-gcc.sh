#!/usr/bin/env bash

set -eo pipefail

DIR=$( cd "$(git rev-parse --show-toplevel)"/toolchain && pwd)

echo "$DIR"

# Note: The version number and hash in BuildClang.sh needs to be kept in sync with this.
BINUTILS_VERSION="2.37"
BINUTILS_MD5SUM="1e55743d73c100b7a0d67ffb32398cdb"
BINUTILS_NAME="binutils-$BINUTILS_VERSION"
BINUTILS_PKG="${BINUTILS_NAME}.tar.gz"
BINUTILS_BASE_URL="https://ftp.gnu.org/gnu/binutils"

# Note: If you bump the gcc version, you also have to update the matching
#       GCC_VERSION variable in the project's root CMakeLists.txt
GCC_VERSION="11.2.0"
GCC_MD5SUM="dc6886bd44bb49e2d3d662aed9729278"
GCC_NAME="gcc-$GCC_VERSION"
GCC_PKG="${GCC_NAME}.tar.gz"
GCC_BASE_URL="https://ftp.gnu.org/gnu/gcc"

ARCH=${ARCH:-"i686"}
TARGET="$ARCH-pc-sakura"
PREFIX="$DIR/local/$ARCH"
BUILD="$DIR/../build/$ARCH"
SYSROOT="$BUILD/root"

MAKE="make"
MD5SUM="md5sum"
NPROC="nproc"
REALPATH="realpath"

SYSTEM_NAME="$(uname -s)"

if command -v ginstall &>/dev/null; then
    INSTALL=ginstall
else
    INSTALL=install
fi

# We *most definitely* don't need debug symbols in the linker/compiler.
# This cuts the uncompressed size from 1.2 GiB per Toolchain down to about 120 MiB.
# Hence, this might actually cause marginal speedups, although the point is to not waste space as blatantly.
export CFLAGS="-g0 -O2 -mtune=native"
export CXXFLAGS="-g0 -O2 -mtune=native"

if [ ! -d "$BUILD" ] ;then
    mkdir -p "$BUILD"
fi
BUILD=$($REALPATH "$BUILD")

git_patch=
while [ "$1" != "" ]; do
    case $1 in
        --dev )           git_patch=1
                          ;;
    esac
    shift
done

echo PREFIX is "$PREFIX"
echo SYSROOT is "$SYSROOT"

mkdir -p "$DIR/tarballs"

buildstep() {
NAME=$1
shift
"$@" 2>&1 | sed $'s|^|\x1b[34m['"${NAME}"$']\x1b[39m |'
}

# === DEPENDENCIES ===

buildstep dependencies echo "Checking whether make is available..."
if ! command -v ${MAKE:-make} >/dev/null; then
    buildstep dependencies echo "Please install GNU make (for the ${MAKE:-make} tool"
    exit 1
fi

buildstep dependencies echo "Checking whether patch is available..."
if ! command -v patch >/dev/null; then
    buildstep dependencies echo "Please install GNU patch (for the 'patch' tool"
    exit 1
fi

buildstep dependencies echo "Checking whether the C compiler works..."
if ! ${CC:-cc} -o /dev/null -xc - >/dev/null <<'PROGRAM'
int main() {}
PROGRAM
then
    buildstep dependencies echo "Please install a working C compiler"
    exit 1
fi

for lib in gmp mpfr mpc; do
    buildstep dependencies echo "Checking whether the $lib library and headers are available..."
    if ! ${CC:-cc} -l$lib -o /dev/null -xc - >/dev/null <<PROGRAM
#include<$lib.h>
int main() {}
PROGRAM
    then
        buildstep dependencies echo "Please install the $lib library and headers"
        exit 1
    fi
done

# === DOWNLOAD AND PATCH ===

pushd "$DIR/tarballs"
    md5=""
    if [ -e $BINUTILS_PKG ]; then
        md5="$($MD5SUM $BINUTILS_PKG | cut -f1 -d' ')"
        echo "bu md5='$md5'"
    fi
    if [ "$md5" != ${BINUTILS_MD5SUM} ] ;then
        rm -f $BINUTILS_PKG
        curl -LO "$BINUTILS_BASE_URL/$BINUTILS_PKG"
    else
        buildstep binutils echo "Skipped downloading binutils"
    fi

    md5=""
    if [ -e $GCC_PKG ]; then
        md5="$($MD5SUM $GCC_PKG | cut -f1 -d' ')"
        echo "gcc md5='$md5'"
    fi
    if [ "$md5" != ${GCC_MD5SUM} ] ;then
        rm -f $GCC_PKG
        curl -LO "$GCC_BASE_URL/$GCC_NAME/$GCC_PKG"
    else
        buildstep gcc echo "Skipped downloading gcc"
    fi

    if [ -d ${BINUTILS_NAME} ]; then
        rm -rf "${BINUTILS_NAME}"
        rm -rf "$DIR/build/$BINUTILS_NAME"
    fi

    buildstep binutils echo "Extracting binutils..."
    tar -xzf ${BINUTILS_PKG}

    pushd ${BINUTILS_NAME}
    buildstep binutils echo "Patching binutils..."
        if [ "$git_patch" = "1" ]; then
            git init > /dev/null
            git add . > /dev/null
            git commit -am "BASE" > /dev/null
            git apply "$DIR"/patches/binutils.patch > /dev/null
        else
            patch -p1 < "$DIR"/patches/binutils.patch > /dev/null
        fi
        $MD5SUM "$DIR"/patches/binutils.patch > .patch.applied
    popd

    if [ -d ${GCC_NAME} ]; then
        rm -rf "${GCC_NAME}"
        rm -rf "$DIR/build/$ARCH/$GCC_NAME"
    fi

    buildstep gcc echo "Extracting gcc..."
    tar -xzf ${GCC_PKG}

    pushd ${GCC_NAME}
    buildstep gcc echo "Patching gcc..."
        if [ "$git_patch" = "1" ]; then
            git init > /dev/null
            git add . > /dev/null
            git commit -am "BASE" > /dev/null
            git apply "$DIR"/patches/gcc.patch > /dev/null
        else
            patch -p1 < "$DIR"/patches/gcc.patch > /dev/null
        fi
        $MD5SUM "$DIR"/patches/gcc.patch > .patch.applied
    popd
popd

# === COMPILE AND INSTALL ===

rm -rf "$PREFIX"
mkdir -p "$PREFIX"

if [ -z "$MAKEJOBS" ]; then
    MAKEJOBS=$($NPROC)
fi

mkdir -p "$DIR/build/$ARCH"

pushd "$DIR/build/$ARCH"
    unset PKG_CONFIG_LIBDIR

    rm -rf binutils
    mkdir -p binutils

    pushd binutils
       buildstep "binutils/configure" "$DIR"/tarballs/$BINUTILS_NAME/configure \
                                         --prefix="$PREFIX" \
                                         --target="$TARGET" \
                                         --with-sysroot="$SYSROOT" \
                                         --enable-shared \
                                         --disable-nls || exit 1
        buildstep "binutils/build" "$MAKE" -j "$MAKEJOBS" || exit 1
        buildstep "binutils/install" "$MAKE" install || exit 1
    popd

    buildstep "system_headers" echo "sakura libc headers"
    mkdir -p "$BUILD"
    pushd "$BUILD"
        mkdir -p root/usr/include/
        SRC_ROOT=$($REALPATH "$DIR"/..)
        FILES=$(find "$SRC_ROOT"/libs/libc "$SRC_ROOT"/libs/libm -name '*.h' -print)
        for header in $FILES; do
            target=$(echo "$header" | sed -e "s@$SRC_ROOT/libs/libc/include@@" -e "s@$SRC_ROOT/libs/libm/include@@")
            buildstep "system_headers" $INSTALL -D "$header" "root/usr/include/$target"
        done
        unset SRC_ROOT
    popd

    rm -rf gcc
    mkdir -p gcc

    pushd gcc
        buildstep "gcc/configure" "$DIR/tarballs/gcc-$GCC_VERSION/configure" --prefix="$PREFIX" \
                                            --target="$TARGET" \
                                            --with-sysroot="$SYSROOT" \
                                            --with-newlib\
                                            --disable-nls \
                                            --enable-shared \
                                            --enable-languages=c,c++ \
                                            --enable-lto || exit 1
        buildstep "gcc/build" "$MAKE" -j "$MAKEJOBS" all-gcc || exit 1
        buildstep "libgcc/build" "$MAKE" -j "$MAKEJOBS" all-target-libgcc || exit 1
        buildstep "gcc+libgcc/install" "$MAKE" install-gcc install-target-libgcc || exit 1
        buildstep "libstdc++/build" "$MAKE" -j "$MAKEJOBS" all-target-libstdc++-v3 || exit 1
        buildstep "libstdc++/install" "$MAKE" install-target-libstdc++-v3 || exit 1
    popd
popd
